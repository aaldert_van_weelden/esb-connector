# ESB Connector #

The Enterprise Service Bus Connector repository, see the ESB repository also

### Home of the ESB connector package ###

* Include this package into your producer or consumer application project manually, or use composer to do so, which is the preferred method. ESB Connector is the connector used by applications to connect  with the ESB Pipeline. 
* Version 1.0

### Dependencies ###

* VWIT foundation

Aaldert van Weelden