<?php
/**
 * Singleton Application class, keeps the application state and provides stateful data around the applicaton
 *
 * @author Aaldert van Weelden 2012
 */

if(!defined("DOC_ROOT")){
	define("DOC_ROOT",dirname(__FILE__).DIRECTORY_SEPARATOR);
}

require_once DOC_ROOT.'include/config.php';//development and production configuration
require_once DOC_ROOT.'bootstrap/autoload.php';

/*
 |--------------------------------------------------------------------------
 | Detect The Application Environment
 |--------------------------------------------------------------------------
 |
 | Specify the environment name in the .env_name.php file. 
 | This will load the appropriate environment .env file
 |
 */
if(!defined ('ENVIRONMENT')){
	if (file_exists( DOC_ROOT.'.env_name.php') ){
		define ('ENVIRONMENT', include(DOC_ROOT.'.env_name.php') );
	} else {
		define ('ENVIRONMENT', 'production');
	}
}

$env = DOC_ROOT.'.env'.(ENVIRONMENT!=='production'?'.'.ENVIRONMENT:'').'.php';
if (!file_exists( $env ) ){
	print 'ERROR: '.$env.' file does not exist';
	exit();
}

if(is_array($_ENV)){
	$_ENV = array_merge($_ENV, require_once $env);
}else{
	$_ENV = require_once $env;
}

//*---------------------------------------------------------------------------


class Application {

	const TAG = "Application";
	const LANGUAGE = "lang";
	public static $instance=null;

	/**
	 * private constructor to implement singleton pattern
	 * @param boolean $xhr
	 * 		Set to true to prevent javascript output when no valid user is logged in while generating JSON XHR responses
	 */
	private function __construct($xhr=false){
		if(!isset(self::$instance)) {
			self::$instance=$this;
		}		

	}


	/**
	 * retrieve the static singleton application instance.
	 *
	 * @param $xhr
	 * 		Set to true to prevent javascript output if no user is logged in during JSON XHR response
	 */
	public static function getInstance($xhr=false) {
	    if(!isset(self::$instance)) {
	      self::$instance = new Application($xhr);
	    }
	    return self::$instance;
	}


	public function getVersion(){
		return APPLICATION_VERSION;
	}

}
?>