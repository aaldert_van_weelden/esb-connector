<?php

/*
 |--------------------------------------------------------------------------
 | Register The Composer Auto Loader
 |--------------------------------------------------------------------------
 |
 | Composer provides a convenient, automatically generated class loader
 | for our application. We just need to utilize it! We'll require it
 | into the script here so that we do not have to worry about the
 | loading of any our classes "manually".
 |
 */
$path = '..';

//needed for unit tests
if(defined('DOC_ROOT')){
	$path = DOC_ROOT;
}

require_once $path.'/vendor/autoload.php';

/*
 |--------------------------------------------------------------------------
 | Register other classes
 |--------------------------------------------------------------------------
 |
 | register classes manually here
 |
 */


?>
