<?php namespace EsbCore\Connector;

use Utils\Logger\LoggerInstance;
use EsbCore\Entities\EnvironmentKey;
use Utils\Util;
use \EsbResponse;
use \EsbMessage;
use \JwtRequest;
use \JwtResponse;
use \CurlAuth;
use \EsbMessageStatus;
use Utils\Logger\Logger;
use \EsbResponseStatus;
use XHR\HTTPcodes;

/**
 * Generic Consumer class, used for recieving messages from the ESB pipeline
 */

class ConsumerConnector extends EsbConsumer{
	
	const TAG = 'ConsumerConnector';
	
	private $log;
	private $callback;
	private $request;
	private $envelope;
	
	public function __construct($callback = null, $endpoint = null, CurlAuth $auth = null){
		$this->log = new LoggerInstance(self::TAG);	
		$this->callback = $callback;
		$this->response = new EsbResponse($_ENV[EnvironmentKey::CONSUMER_KEY]);
		$this->request = new JwtRequest();
		$this->envelope = new JwtResponse();
		
		if(Util::notNullOrEmpty($endpoint)){
			$this->EsbConsumerEndpoint = $endpoint;
		}else{
			$this->EsbConsumerEndpoint =  $_ENV[EnvironmentKey::ESB_CONSUMER_ENDPOINT];
		}
		if(Util::notNullOrEmpty($auth)){
			$this->auth = $auth;
		}else{
			$this->auth = new CurlAuth($_ENV[EnvironmentKey::CONSUMER_KEY], $_ENV[EnvironmentKey::CONSUMER_TOKEN]);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(){
		
		if( !(isset($_SERVER['PHP_AUTH_USER']) && 
					$_SERVER['PHP_AUTH_USER']===$_ENV[EnvironmentKey::ESB_KEY] && 
					$_SERVER['PHP_AUTH_PW']===$_ENV[EnvironmentKey::ESB_TOKEN]) )
		{
			\ExceptionHelper::sendExceptionJsonResponse(new \AuthenticationException(self::TAG.': Acces not allowed, wrong credentials'));
		}
		
		switch($_SERVER['REQUEST_METHOD']){
			case 'GET':
				$this->response->setStatus(new EsbMessageStatus(EsbMessageStatus::REJECTED) );
				$this->sendResponse(HTTPcodes::HTTP_BAD_REQUEST);
				
				break;
				
			case 'POST':
				$this->log->trace('Consumer: trying to digest the encoded ESB_PIPELINE message');
				
				try {
					
					//$this->log->out(file_get_contents ( "php://input" ));
					
					$this->message = $this->request->decodeAndDecrypt( file_get_contents ( "php://input" ) );
					
					//$this->log->out($this->message);
					
					$this->response->setMessageID($this->message->getID());
					
					if( is_callable($this->callback) ) return call_user_func($this->callback, $this->message);
					
					return $this->message;
		
						
				} catch (Exception $e) {
					\ExceptionHelper::sendExceptionJsonResponse(new \ParseException('Message has wrong format: '.$e->getMessage()));
				}
				break;
				//illegal mehods, only POST and GET are allowed
			default:
				\ExceptionHelper::sendExceptionJsonResponse(new \BadRequestException('Wrong method: HTTP_'.$_SERVER['REQUEST_METHOD'].' , access not allowed'));
				break;
		}
	}
		
	/**
	 * Accept the message and return a payload
	 * @param EsbMessage $message The original message
	 * @param mixed $payload
	 */
	public function accept(\EsbMessage $message, $payload){
		Logger::log()->debug('ConsumerPipelineConnector:accept: sending response for message['.$message->getID().']');
		
		$response = new \EsbResponse($this->auth->applicationKey);
		$response->setMessageID($message->getID());
		$response->setPayload($payload);
		$response->setSuccess(EsbMessage::SUCCESS);
		$response->setStatus( new EsbResponseStatus(EsbResponseStatus::HTTP_ACCEPTED));
		
		$this->envelope->encode($response);
		$this->envelope->sendJSON();
	}
	
	/**
	 * Reject the message with an error and an optional payload
	 * @param EsbMessage $message   The original message
	 * @param mixed $error
	 * @param mixed $payload (optional)
	 */
	public function reject(\EsbMessage $message, $error, $payload=null){
		Logger::log()->debug('ConsumerPipelineConnector:reject: sending response for message['.$message->getID().']');
		
		$response = new \EsbResponse($this->auth->applicationKey);
		$response->setMessageID($message->getID());
		$response->setPayload($payload);
		$response->setSuccess(EsbResponse::FAILURE);
		$response->setStatus( new EsbResponseStatus(EsbResponseStatus::HTTP_REJECTED) );
		$response->setError($error);
		
		$this->envelope->encode($response);
		$this->envelope->sendJSON();
	}
	
}
?>