<?php namespace EsbCore\Connector;
/**
 * Use this class to bind ESB fire-and-forget events, without instantiating new connectors
 * @author aaldert.vanweelden
 *
 * <pre>
 * Example 1:
 * ESB::bind('blog.post.create', function(EsbMessage $message) {
 *    $self->dispatch ( $message );
 * });
 *
 * Example 2:
 * ESB::fire('my.event', EsbMessage $message);
 * </pre>
 *
 */
class ESB {

	public static $events = [];

	/**
	 * Fire the registered event
	 * @param string $event The event name
	 * @param \EsbPayload $payload The event payload
	 */
	public static function fire($event, \EsbPayload $payload) {
		if (isset(self::$events[$event])) {

			foreach (self::$events[$event] as $func) {

				return call_user_func($func, $payload);
			}
		}
	}

	/**
	 * Bind the event with the provided name
	 * @param string $event
	 * @param \Closure $func
	 */
	public static function bind($event, \Closure $func) {
		self::$events[$event][] = $func;
	}
}