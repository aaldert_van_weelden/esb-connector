<?php namespace EsbCore\Connector;


/**
 * Class used to make the connection between the consumer application and the ESB.
 * Place generic connector methods, constants and properties here if neccesary
 * Extend from this class in your project, do not use it directly
 * 
 * @author Aaldert van Weelden
 *
 */
abstract class EsbConsumer {
	
	/**
	 * The response to send
	 * @see EsbCore\Connector\EsbConsumer
	 * @var \EsbResponse
	 */
	protected $response;
	
	/**
	 * The message recieved from the ESB Pipeline
	 * @see EsbCore\Connector\EsbConsumer
	 * @var \EsbMessage
	 */
	protected $message;
	
	/**
	 * The credentials as used for the HTTP Basic Auth
	 * @see EsbCore\Connector\EsbConsumer
	 * @var \CurlAuth
	 */
	protected $auth;
	
	/**
	 * Endpoint as specified in the consumer .env config file
	 * @see EsbCore\Connector\EsbConsumer
	 * @var string
	 */
	protected $EsbConsumerEndpoint;
	
	/**
	 * Send the JSON encoded response
	 * @param \HTTPcodes $httpCode
	 * @return void, script exits after header and string output
	 */
	protected function sendResponse( $httpCode ){
	
		$content =  $this->response->encodeJSON();
	
		http_response_code ( $httpCode );
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header('Content-Length:', strlen($content));
			
		print $content;
		exit();
	}
	
	
	/**
	 * @return \EsbMessage
	 */
	public function getMessage(){
		return $this->message;
	}
	
	/**
	 * @return \EsbResponse
	 */
	public function getResponse(){
		return $this->response;
	}
	
	/**
	 * @return \CurlAuth
	 */
	public function getAuth(){
		return $this->auth;
	}
	
	/**
	 * @return string The consumer endpoint
	 */
	public function getEndpoint(){
		return $this->EsbConsumerEndpoint;
	}
}
