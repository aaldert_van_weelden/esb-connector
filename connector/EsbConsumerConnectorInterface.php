<?php namespace EsbCore\Connector;


interface EsbConsumerConnectorInterface {
	
	/**
	 * Digest the incoming message recieved from the ESB Pipeline or the ESB Proxy channels
	 * @method POST
	 */
	public function digest();
	
}
