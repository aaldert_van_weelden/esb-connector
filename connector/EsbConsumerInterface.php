<?php namespace EsbCore\Connector;

/**
 * Implement this interface to enable the connection with the ESB Pipeline and the Consumer
 * @author Aaldert van Weelden
 */
interface EsbConsumerInterface{
	
	/**
	 * Handle the message transmitted by HTTP POST request from the ESB Pipeline
	 * @param $callback The callback function, optional
	 * @return \EsbResponse $response. The response status
	 */
	public function handleMessage(callable $callback);
	
	/**
	 * Transmit the Consumer Message status by sending an /EsbResponse object by POST HTTP request to the ESB pipeline
	 * @return Enum $status  The message status
	 */
	public function setStatus();
	
}

?>