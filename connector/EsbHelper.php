<?php
namespace EsbCore\Helper;

use InvalidArgumentException;
/**
 * Container for ESB helper functions
 * @author Aaldert van Weelden
 *
 */

class EsbHelper{
	
	public static function isEncrypted($request){
		if(is_array($request) ){
			$request = (object) $request;
			if( !property_exists($request, 'type')  ) throw new \PropertyException('The request does not contain a type');
			if($request->type == 'JWT_ENCRYPTED') return true;
			return false;
		}
		throw new \InvalidArgumentException('provided request must be an array');
	}
}