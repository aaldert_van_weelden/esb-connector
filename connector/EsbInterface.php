<?php namespace EsbCore\Connector;
/**
 * Interface for exposing the Esb controller methodes
 * @author Aaldert van Weelden
 *
 */
interface EsbInterface{
	
	/**
	 * Process the message recieved by HTTP POST Basic Auth from the Producer
	 * @param EsbMessage $messag The revived message
	 * @return \EsbResponse The JSON status response
	 */
	public function handleMessageFromProducer();
	
	
	/**
	 * Process the message recieved by HTTP POST Basic Auth from the Proxy Producer
	 * @param EsbMessage $messag The revived message
	 * @return \EsbResponse The JSON status response
	 */
	public function handleMessageFromProxyProducer();
	
	/**
	 * Transmit the provided message to the provided consumer, identified with the Application UUID
	 * 
	 * @param \EsbMessage $message The message object
	 * @param unknown $consumerID The unique consumer ID (token)
	 * @return \EsbResponse The JSON status response
	 */
	public function transmitMessageToConsumer(\EsbMessage $message, $consumerID);
	
	
	/**
	 * Set the Consumer Application Message status  (as prescribed by the Producer and available as enumeration in the Esb connector)
	 * in the ESB database by HTTP POST request by the consumer
	 * @param string $id The message UUID
	 * @return \EsbResponse The JSON status response
	 */
	public function setConsumerMessageStatus($id);
	
	/**
	 * Retrieve the Consumer Application Message status (as prescribed by the Porducer and available as enumeration in the Esb connector)
	 * from the ESB database
	 *
	 * @param string $messageID  The unique messageID
	 * @return \EsbResponse The JSON status response
	 */
	public function getConsumerMessageStatus($messageID);
	
}


?>