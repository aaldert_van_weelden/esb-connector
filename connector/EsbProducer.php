<?php namespace EsbCore\Connector;


/**
 * Class used to make the connection between the producer application and the ESB
 * Extend from this class in your project, do not use it directly
 * Place generic connector methods, constants and properties here if neccesary
 * 
 * @author Aaldert van Weelden
 *
 */
abstract class EsbProducer implements EsbProducerInterface{
	
	
	/**
	 * The response to return
	 * @see EsbCore\Connector\EsbConsumer
	 * @var \EsbResponse
	 */
	protected $response;
	
	/**
	 * The message recieved from the ESB Pipeline
	 * @see EsbCore\Connector\EsbConsumer
	 * @var \EsbMessage
	 */
	protected $message;
	
	/**
	 * The credentials as used for the HTTP Basic Auth
	 * @see EsbCore\Connector\EsbConsumer
	 * @var \CurlAuth
	 */
	protected $auth;

	/**
	 * Endpoint as specified in the Producer .env config file
	 * @var string
	 */
	protected $EsbProducerEndpoint;
	/**
	 * Producer key as specified in the ESB Pipeline database and available in the producer .env config file. 
	 * Used for authentication
	 * @var string
	 */
	protected $producerKey;
	/**
	 * The producer token as specified in the ESB Pipeline database as UUID and available in the producer .env config file. 
	 * Used for authentication
	 * @var string
	 */
	protected $producerToken;
	
	/**
	 * @return \EsbMessage
	 */
	public function getMessage(){
		return $this->message;
	}
	
	/**
	 * @return \EsbResponse
	 */
	public function getResponse(){
		return $this->response;
	}
	
	/**
	 * @return \CurlAuth
	 */
	public function getAuth(){
		return $this->auth;
	}

	/**
	 * @return string The producer endpoint
	 */
	public function getEndpoint(){
		return $this->EsbProducerEndpoint;
	}
	
}
