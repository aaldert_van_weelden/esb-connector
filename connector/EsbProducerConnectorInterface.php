<?php namespace EsbCore\Connector;


interface EsbProducerConnectorInterface {
	
	/**
	 * Implement this method to transmit a callback message to the ESB when the callback endpoint is called
	 * @param \EsbPayload $payload
	 * @method POST
	 */
	public function dispatchCallback(\EsbPayload $payload);
	
	
	
	/**
	 * Implement this method to transmit a test message to the ESB proxy when the callback endpoint is called
	 * @method GET
	 */
	public function dispatchTestMessage();
	
	
}