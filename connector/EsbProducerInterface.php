<?php namespace EsbCore\Connector;

/**
 * Implement this interface to enable the connection with the ESB Pipeline and the Producer
 * @author Aaldert van Weelden
 */
interface EsbProducerInterface{
	
	/**
	 * Dispatch the message to the ESB Pipeline by HTTP POST request
	 * @param \EsbMessage $message
	 * @param $callback The callback function, optional
	 * $return \EsbResponse $response The 
	 */
	public function dispatch(\EsbMessage $message, callable $callback = null);
	
	/**
	 * Retrieve the Producer prescribed Consumer status from the ESB Pileline
	 * @param string $messageID
	 * @return Enum $status The status enumeration
	 */
	public function getConsumerStatus($messageID);
	
}

?>