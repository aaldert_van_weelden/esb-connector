<?php namespace EsbCore\Connector;

use Utils\Logger\LoggerInstance;
use Utils\Util;
use EsbCore\Entities\EnvironmentKey;
use \CurlAuth;
use \CurlHelper;
use \EsbMessage;
use \EsbResponse;
use \JwtRequest;
use \CurlMethod;


/**
 * Producer base class, used to send signed and encrypted messages to the ESB Pipeline
 */

class ProducerPipelineConnector extends EsbProducer{
	
	const TAG = 'ProducerPipelineConnector';
	
	
	
	protected $log;
	
	protected static $instance = null;
	protected static $bound = false;
	
	public function __construct($endpoint = null, CurlAuth $auth = null){
		$this->log = new LoggerInstance(self::TAG);
	
		if(Util::notNullOrEmpty($endpoint)){
			$this->EsbProducerEndpoint = $endpoint;
		}else{
			$this->EsbProducerEndpoint = $_ENV[EnvironmentKey::ESB_PRODUCER_ENDPOINT];
		}
	
		if(Util::notNullOrEmpty($auth)){
			$this->auth = $auth;
		}else{
			$this->auth = new CurlAuth($_ENV[EnvironmentKey::PRODUCER_KEY], $_ENV[EnvironmentKey::PRODUCER_TOKEN]);
		}
	}	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch( \EsbMessage $message, callable $callback = null){
		$this->log->debug('ProducerConnector::dispatch Trying to send the  message with id['.$message->getID().']');
		$this->log->debug('endpoint: '.$this->EsbProducerEndpoint);
		$this->log->dump($this->auth,'AUTH');
		
		$envelope = new JwtRequest();
		$encoded = $envelope->encodeAndEncrypt($message);
		
		$connector = new CurlHelper(CurlMethod::POST,$this->EsbProducerEndpoint, $envelope, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		$this->response = new EsbResponse();
		$this->response->revive( (array) $connector->getResponseObject());
		
		if( is_callable($callback) ) return call_user_func($callback,$this->response);
		
		return $this->response;
		
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){
		$this->log->debug('ProducerConnector::getConsumerStatus  Trying to retrieve the consumer application message status for id['.$messageID.']');
		
		$this->message = new EsbMessage(null, null, $this->auth->applicationKey);
		$this->message->setID($messageID);
		
		$connector = new CurlHelper(CurlMethod::GET,$this->EsbProducerEndpoint, $this->message, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		$response = $connector->getResponse();
		
		return $response->status;
		
	}
	
	
	
	public static function instance() {
		if(!isset(self::$instance)) {
			self::$instance = new ProducerPipelineConnector();
		}
		return self::$instance;
	}


	
	public static function bind(){
		
	}
	
	
}

?>