<?php namespace EsbCore\Connector;

use Utils\Logger\LoggerInstance;
use EsbCore\Entities\EnvironmentKey;
use EsbCore\Entities\AppConstants;
use Utils\Util;
use \CurlAuth;
use \CurlHelper;
use \EsbMessage;
use \EsbResponse;
use \JwtRequest;
use \JwtResponse;
use \CurlMethod;


/**
 * Producer base class, used to send signed and encrypted messages to the ESB Proxy
 */

class ProducerProxyConnector extends EsbProducer{
	
	const TAG = 'ProducerProxyConnector';
	
	
	
	private $log;
	
	public function __construct($endpoint = null, CurlAuth $auth = null){
		$this->log = new LoggerInstance(self::TAG);
		
		if(Util::notNullOrEmpty($endpoint)){
			$this->EsbProducerEndpoint = $endpoint;
		}else{
			$this->EsbProducerEndpoint = $_ENV[EnvironmentKey::ESB_PROXY_ENDPOINT];
		}
		
		if(Util::notNullOrEmpty($auth)){
			$this->auth = $auth;
		}else{
			$this->auth = new CurlAuth($_ENV[EnvironmentKey::PRODUCER_KEY], $_ENV[EnvironmentKey::PRODUCER_TOKEN]);
		}
	}
	
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch( \EsbMessage $message, callable $callback = null){
		$this->message = $message;
		$this->log->debug('ProducerProxyConnector::dispatch Trying to send the  message with id['.$this->message->getID().']');
		
		$envelope = new JwtRequest();
		$encoded = $envelope->encodeAndEncrypt($this->message);
		
		$connector = new CurlHelper(CurlMethod::POST,$this->EsbProducerEndpoint, $envelope, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();

		$envelope = new JwtResponse();
		$this->response = $envelope->decode($connector->getResponse());
		
		if( is_callable($callback) ) return call_user_func($callback,$this->response);
		
		return $this->response;
		
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){
		$this->log->debug('ProducerProxyConnector::getConsumerStatus  Trying to retrieve the consumer application message status for id['.$messageID.']');
		
		$message = new EsbMessage(null, null, $this->auth->applicationKey);
		$message->setID($messageID);
		
		$connector = new CurlHelper(CurlMethod::GET,$this->EsbProducerEndpoint, $message, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		$response = $connector->getResponse();
		return $response->status;
		
	}
	
	
}

?>