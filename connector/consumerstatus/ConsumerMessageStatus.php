<?php  namespace EsbCore\Connector\ConsumerStatus;
use VWIT\Base\Enum;
/**
 * Enum used as a base reference to the status of the application message.
 * Do not use this class directly but create an application specific enum which extends from this class
 * 
 * @author Aaldert van Weelden
 *
 */
abstract class ConsumerMessageStatus extends Enum {
	
  const UNKNOWN 	= 'unknown';
  const BUSY 		= 'busy';
  const CLOSED 		= 'closed'; 
  const REJECTED 	= 'rejected';
  const CANCELLED 	= 'cancelled';
 

  //default value
  public $value = self::UNKNOWN;
  
  /**
   * Retrieve the translated value for display purposes
   * @return string $value The i18n value
   */
  public function translate(){
	  	if(function_exists('trans')){
	  	 	$this->setI18n( trans('esb_enums.'.$this->name()) );
	  	 	return $this->i18nValue();
	  	}
	  	$this->setI18n($this->name());
	  	return $this->name();
  }
  
  /* (non-PHPdoc)
   * @see EnumInterface::get()
   */
  public static function get($value = null){
  	return new ConsumerMessageStatus($value);
  }

}
