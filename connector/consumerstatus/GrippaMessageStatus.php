<?php  namespace EsbCore\Connector\ConsumerStatus;

/**
 * Enum used as a reference to the status of the application message.
 * Used for testing only
 * @author Aaldert van Weelden
 *
 */
class GrippaMessageStatus extends ConsumerMessageStatus {
	
	/* (non-PHPdoc)
	 * @see EnumInterface::get()
	 */
	public static function get($value = null){
		return new GrippaMessageStatus($value);
	}

}
