<?php  namespace EsbCore\Connector\ProducerStatus;

use VWIT\Base\Enum;

/**
 * Enum used as a base reference to the status of the application message.
 * 
 * @author Aaldert van Weelden
 *
 */
class ProducerMessageStatus extends Enum {
	
  const UNKNOWN 		= 'unknown';
  const TEST 			= 'test';
  const LOW_PRIORITY 	= 'low-priority'; 
  const HIGH_PRIORITY 	= 'high-priority';
 

  //default value
  public $value = self::UNKNOWN;
  
  /**
   * Retrieve the translated value for display purposes
   * @return string $value The i18n value
   */
  public function translate(){
  	 if(function_exists('trans')){
	  	 	$this->setI18n( trans('esb_enums.'.$this->name()) );
	  	 	return $this->i18nValue();
	  }
	  $this->setI18n($this->name());
	  return $this->name();
  }
  
  public static function get($value = null){
  	return new ProducerMessageStatus($value);
  }

}
