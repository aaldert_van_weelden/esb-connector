<?php namespace EsbCore\Entities;
/**
 * Class used to define ESB Connector application constants
 * @author Aaldert van Weelden
 *
 */
class AppConstants{
	
	/**
	 * Enable the JWT payload RSA encryption
	 * @var boolean
	 */
	const ENABLE_RSA_ENCRYPTION  = true;
	/**
	 * Enable the JWT payload RSA encryption
	 * @var boolean
	 */
	const DISABLE_RSA_ENCRYPTION  = false;
	
	const ENABLE_QUERY_LOGGING = true;
	const DISABLE_QUERY_LOGGING = false;
}