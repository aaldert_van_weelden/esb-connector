<?php  namespace EsbCore\Entities;
use VWIT\Base\Enum;
/**
 * Enum used as a reference for the ESB keys used in the .env config files .
 * 
 * @author Aaldert van Weelden
 *
 */
class EnvironmentKey extends Enum {
	
  const UNKNOWN = null;
  
  const ESB_PRODUCER_ENDPOINT = 'esb.producer.endpoint';
  const PRODUCER_KEY = 'producer.key';
  const PRODUCER_TOKEN = 'producer.token'; 
  
  const ESB_CONSUMER_ENDPOINT = 'esb.consumer.endpoint';
  const CONSUMER_KEY = 'consumer.key';
  const CONSUMER_TOKEN = 'consumer.token';
  
  const ESB_PROXY_ENDPOINT = 'esb.proxy.endpoint'; 
  
  const ESB_KEY = 'esb.key';
  const ESB_TOKEN = 'esb.token';
  
  const RSA_KEYFILES = 'rsa.keyfiles';
  const SERVER_NAME = 'server.name';
  const JWT_SECRET = 'jwt.secret';
  const JWT_RSA_ENCRYPT = 'jwt.rsa.encrypt';
  
  const FIDDLER_PROXY = 'fiddler.proxy';

  //default value
  public $value = self::UNKNOWN;
  
  /* (non-PHPdoc)
   * @see EnumInterface::get()
   */
  public static function get($value = null){
  	return new EnvironmentKey($value);
  }

}
