<?php
use VWIT\Base\Enum;

/**
 * Enum used as a reference to the status of the ESB message.
 * @author Aaldert van Weelden
 *
 */
class EsbMessageStatus extends Enum {
	
  const UNKNOWN = 'UNKNOWN';
  const ACCEPTED = 'ACCEPTED';
  const REJECTED = 'REJECTED';
 
  //default value
  public $value = self::UNKNOWN;
  
  
 /**
   * Retrieve the translated value for display purposes
   * @return string $value The i18n value
   */
  public function translate(){
	  	if(function_exists('trans')){
	  	 	$this->setI18n( trans('esb_enums.'.$this->name()) );
	  	 	return $this->i18nValue();
	  	}
	  	$this->setI18n($this->name());
	  	return $this->name();
  }
  
  /* (non-PHPdoc)
   * @see EnumInterface::get()
   */
  public static function get($value = null){
  	return new EsbMessageStatus($value);
  }

}
