<?php
use Utils\Util;
use XHR\JSON;

/**
 * Class used to encapsulate the payload and convert it to a JSON object
 * @author Aaldert van Weelden
 *
 */
class EsbPayload extends JSON{
	
	
	public $id;
	
	/**
	 * The constructor
	 */
	public function __construct(){
		parent::__construct();
		$this->id = Util::create_GUID();
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading conversion
	 */
	public function toObject() {
		return (object) array_merge(get_object_vars($this));
	
	}
	
	
}