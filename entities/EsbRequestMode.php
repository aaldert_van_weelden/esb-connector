<?php
use VWIT\Base\Enum;

/**
 * Enum used as a  reference to the direction of the requests. The sender is leading here.
 * 
 * @author Aaldert van Weelden
 *
 */
class EsbRequestMode extends Enum {
	
  const UNKNOWN 			= 'unknown';
  const SENT_BY_ESB 		= 'sent.by.esb';
  const SENT_BY_PRODUCER 	= 'sent.by.producer'; 
  const SENT_BY_CONSUMER 	= 'sent.by.consumer';
 
  
 

  //default value
  public $value = self::UNKNOWN;
  
  /**
   * Retrieve the translated value for display purposes
   * @return string $value The i18n value
   */
  public function translate(){
	  	if(function_exists('trans')){
	  	 	$this->setI18n( trans('esb_enums.'.$this->name()) );
	  	 	return $this->i18nValue();
	  	}
	  	return $this->name();
  }
  
  /* (non-PHPdoc)
   * @see EnumInterface::get()
   */
  public static function get($value = null){
  	return new EsbRequestMode($value);
  }

}
