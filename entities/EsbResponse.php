<?php
use Utils\Util;
use XHR\JSON;

/**
 * Class used to encapsulate the response and convert it to a JSON object
 * @author Aaldert van Weelden
 *
 */
class EsbResponse extends CurlMessage{
	
	const SUCCESS = true;
	const FAILURE = false;
	
	/**
	 * The response UUID
	 * @var string
	 */
	protected $id;
	
	/**
	 * The message id, if present
	 * @var string
	 */
	protected $message_id;
	
	/**
	 * The response sender. This is a predefined application key
	 * @var string
	 */
	protected $sender;
	/**
	 * The status enumeration. This can be a message status or a consumer application status which is prescribed by the producer 
	 * and is returned by the consumer when requested for
	 * @var Enum
	 */
	protected $status;
	/**
	 * The message payload. This can be anything.
	 * @var mixed
	 */
	protected $payload;
	
	/**
	 * The constructor
	 */
	public function __construct($sender = null, $messageID = null, $success = self::SUCCESS){
		parent::__construct([], $success);
		$this->id = Util::create_GUID();
		$this->sender = $sender;
		$this->status = null;
		$this->payload = null;
	}
	

	public function getID(){
		return $this->id;
	}
	
	public function setID($id){
		$this->id=$id;
	}
	
	
	public function getMessageID(){
		return $this->message_id;
	}
	
	public function setMessageID($messageID){
		$this->message_id=$messageID;
	}

	public function setSender($sender){
		$this->sender=$sender;
	}
	
	public function getSender(){
		return $this->sender;
	}
	
	public function getPayload(){
		return $this->payload;
	}
	
	public function setPayload($payload){
		$this->payload = $payload;
	}
	
	/**
	 * Get the status. This can be the ESB response status, or the Producer prescribed Consumer status
	 * @return Status the status Enum
	 */
	public function getStatus(){
		return $this->status;
	}
	
	/**
	 * Set the status.
	 * @param Status $status
	 */
	public function setStatus($status){
		$this->status=$status;
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
}