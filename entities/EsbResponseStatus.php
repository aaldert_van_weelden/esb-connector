<?php
use VWIT\Base\Enum;
/**
 * Enum used as a  reference to the status of the consumer response.
 * 
 * @author Aaldert van Weelden
 *
 */
class EsbResponseStatus extends Enum {

  const HTTP_OK							= 200;
  const HTTP_ACCEPTED 					= 202;
  const HTTP_NO_CONTENT 				= 204; 
  const HTTP_BAD_REQUEST				= 400;
  const HTTP_UNAUTHORIZED 				= 401;
  const HTTP_NOT_FOUND 					= 404;
  const HTTP_REJECTED					= 450;
  const HTTP_PAYLOAD_TOO_LARGE   		= 413;
  const HTTP_INTERNAL_SERVER_ERROR		= 500;
  
 

  //default value
  public $value = self::HTTP_OK;
  
  /**
   * Retrieve the translated value for display purposes
   * @return string $value The i18n value
   */
  public function translate(){
	  	if(function_exists('trans')){
	  	 	$this->setI18n( trans('esb_enums.'.$this->name()) );
	  	 	return $this->i18nValue();
	  	}
	  	return $this->name();
  }
  
  /* (non-PHPdoc)
   * @see EnumInterface::get()
   */
  public static function get($value = null){
  	return new EsbResponseStatus($value);
  }

}
