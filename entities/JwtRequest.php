<?php
use EsbCore\Entities\EnvironmentKey;
use Firebase\JWT\JWT;
use Utils\Util;
use Utils\Logger\Logger;

/**
 * Class used to wrap the payload into a unique request and convert it to a encrypted JSON object 
 * and sign the request as a JSON  Web Token.
 * @author Aaldert van Weelden
 *
 */
class JwtRequest extends CurlMessage{
	
	/**
	 * The message UUID id
	 * @var string
	 */
	protected $id;
	protected $type;
	protected $data;
	protected $success;
	protected $error;
	
	/**
	 * The constructor
	 */
	public function __construct($success = self::SUCCESS){
		parent::__construct(['id', 'success'], $success);
		$this->id = Util::create_GUID();
		$this->type = 'JWT';
		
		if(!isset($_ENV[EnvironmentKey::JWT_SECRET])) throw new \InvalidKeyException('JWT_SECRET key is not present');
		if(!isset($_ENV[EnvironmentKey::JWT_RSA_ENCRYPT])) throw new \InvalidKeyException('JWT_RSA_ENCRYPT key is not present');
		if(!isset($_ENV[EnvironmentKey::RSA_KEYFILES])) throw new \InvalidKeyException('RSA_KEYFILES key is not present');
	}
	
	/**
	 * {@inheritDoc}
	 * @see Message::getID()
	 */
	public function getID(){
		return $this->id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see CurlMessage::setID()
	 */
	public function setID($id){
		$this->id = $id;
	}

	public function getType(){
		return $this->type;
	}
	
	public function setType($type){
		$this->type = $type;
	}
	

	public function getData(){
		return $this->data;
	}
	
	public function setData($data){
		$this->data = $data;
	}
	
	
	/**
	 * JWT encode and RSA encrypt the provided message
	 * @param \EsbMessage $message
	 * @return string The encoded string
	 */
	public function encodeAndEncrypt(\EsbMessage $message){
		$data = null;
		
		//$tokenId    = base64_encode(mcrypt_create_iv(32));//not working
		$tokenId    = base64_encode(Util::create_guid_section(32));
		$issuedAt   = time();
		$notBefore  = $issuedAt;             //Adding 0 seconds
		$expire     = $notBefore + 60;            // Adding 60 seconds
		$serverName = $_ENV[EnvironmentKey::SERVER_NAME]; // Retrieve the server name from config file
		
		Logger::log()->debug('tokenID:'.$tokenId);
		
		if($_ENV[EnvironmentKey::JWT_RSA_ENCRYPT]){
			Logger::log()->debug('enabling RSA encryption');
			$rsa = new Crypt_RSA();
			
			if( !$rsa->loadKey(file_get_contents($_ENV[EnvironmentKey::RSA_KEYFILES].'id_rsa.pub')) ){
				throw new InvalidKeyException('Invalid RSA public key provided');
			}
			$data = bin2hex( $rsa->encrypt( $message->encodeBase64() ) );
		}else{
			$data = $message->encodeBase64();
		}
	
		$data = [
				'iat'  => $issuedAt,         // Issued at: time when the token was generated
				'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
				'iss'  => $serverName,       // Issuer
				'nbf'  => $notBefore,        // Not before
				'exp'  => $expire,           // Expire
				'data' => $data
		];
		$this->data = JWT::encode($data, $_ENV[EnvironmentKey::JWT_SECRET],'HS512');
		return $this->data;
	}
	
	/**
	 * JWT Decode and RSA decrypt the reiceived request data to retrieve the original message object
	 * @param string $rawRequest
	 * @return \EsbMessage  The original message
	 */
	public function decodeAndDecrypt($rawRequest){
		
		$response = null;
		
		if(is_object($rawRequest) || is_array($rawRequest)){
			$request = (array) $rawRequest;
		}else{
			$request = json_decode ( $rawRequest , true );
		}
		
		
		$this->revive($request);
		
		//decode JWT
		$decoded = JWT::decode($this->data, $_ENV[EnvironmentKey::JWT_SECRET],['HS512']);

		if($_ENV[EnvironmentKey::JWT_RSA_ENCRYPT]){
			$ciphertext = hex2bin($decoded->data);
				
			$rsa = new Crypt_RSA();
			if(! $rsa->loadKey( file_get_contents($_ENV[EnvironmentKey::RSA_KEYFILES].'id_rsa') ) ){
				throw new InvalidKeyException('Invalid RSA public key provided');
			}
			$decrypted = $rsa->decrypt($ciphertext);
			$request = json_decode(base64_decode($decrypted), true);
		}else{
			$request = json_decode(base64_decode($decoded->data), true);
		}
		
		$esbMessage = new EsbMessage();
		$esbMessage->revive ($request);
		
		return $esbMessage;
	}
}