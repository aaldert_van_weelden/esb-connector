<?php
use EsbCore\Entities\EnvironmentKey;
use Firebase\JWT\JWT;
use Utils\Util;
use XHR\JSON;

/**
 * Class used to encapsulate the response and convert it to a JSON object
 * @author Aaldert van Weelden
 *
 */
class JwtResponse extends JSON{
	
	const SUCCESS = true;
	const FAILURE = false;
	
	private $id;
	private $data;
	private $success;
	private $error;
	
	/**
	 * The constructor
	 */
	public function __construct($success = self::SUCCESS){
		parent::__construct();
		$this->id = Util::create_GUID();
		$this->data = null;
		$this->success = $success;
		$this->error= null;
		
		if(!isset($_ENV[EnvironmentKey::JWT_SECRET])) throw new \InvalidKeyException('JWT_SECRET key is not present');
	}
	

	public function getID(){
		return $this->id;
	}
	
	public function setID($id){
		$this->id=$id;
	}
	
	public function getData(){
		return $this->data;
	}
	
	public function setData($data){
		$this->data = $data;
	}
	
	public function isSuccess(){
		return $this->success;
	}
	
	public function setSuccess($success){
		$this->success=$success;
	}
	
	public function getError(){
		return $this->error;
	}
	
	public function setError($error){
		$this->error=$error;
	}
	
	/**
	 * Retrieves the merge of object en parent, used for cascading
	 * object to array conversion
	 */
	public function toArray() {
		return array_merge(get_object_vars($this));
	
	}
	
	/**
	 * Revive the original response from the response DTO object
	 *
	 * Ignored properties are clazz, created_at and updated_at
	 *
	 * @param array $request  The request array
	 * @return EsbMessage $message  The revived message
	 */
public function revive( $response){
	
		$response = (object) $response;
	
		
		if(Util::nullOrEmpty($response)){
			$error="provided Response object cannot be null. Probably no valid response recieved.";
			throw new \NullArgumentException($error);
		}
		if(gettype($response)!="object"){
			$error="provided Response object must be of type object, ".gettype($response)." given";
			throw new \InvalidTypeException($error);
		}
	
		$dtoProperties = $this->getProperties();
		$responseProperties= array_keys(get_object_vars($response));
	
		$unsupported = array_diff($responseProperties,$dtoProperties);
	
		if(Util::notNullOrEmpty($unsupported)){
			$error = "Unsupported Response fields detected: ".implode(',',$unsupported);
			throw new \PropertyException($error);
		}
	
		foreach($dtoProperties as $key=>$property){
			if(in_array($property,$responseProperties)){
				$this->{$property} = $response->{$property};
			}
		}
		
		return $this;
	
	}
	
	private function getProperties(){
	
		$properties = array();
		$pairs = array();
		$reflect = new ReflectionClass($this);
		$props = (array) $reflect->getProperties(ReflectionProperty::IS_PRIVATE);
	
		foreach($props as $prop){
			$properties[]=$prop->name;
		}
		return $properties;
	}
	
	/**
	 * JWT encode the provided message
	 * @param \EsbMessage $message
	 * @return string The encoded string
	 */
	public function encode(\EsbResponse $response){
		$data = null;
		
		//$tokenId    = base64_encode(mcrypt_create_iv(32));//not working
		$tokenId    = base64_encode(Util::create_guid_section(32));
		$issuedAt   = time();
		$notBefore  = $issuedAt;             //Adding 0 seconds
		$expire     = $notBefore + 60;            // Adding 60 seconds
		$serverName = $_ENV[EnvironmentKey::SERVER_NAME]; // Retrieve the server name from config file
		
		$data = [
				'iat'  => $issuedAt,         // Issued at: time when the token was generated
				'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
				'iss'  => $serverName,       // Issuer
				'nbf'  => $notBefore,        // Not before
				'exp'  => $expire,           // Expire
				'data' => $response->encodeBase64()
		];
	
		$this->data = JWT::encode($data, $_ENV[EnvironmentKey::JWT_SECRET],'HS512');
		return $this->data;
	}
	
	/**
	 * JWT Decode the reiceived request data to retrieve the original message object
	 * @param string $rawRequest
	 * @return \EsbResponse  The original response
	 */
	public function decode($rawRequest){
		$response = null;
		
		if(is_object($rawRequest) || is_array($rawRequest)){
			$request = (array) $rawRequest;
		}else{
			$request = json_decode ( $rawRequest , true );
		}
		
		$this->revive($request);
		
		//decode JWT
		$decoded = JWT::decode($this->data, $_ENV[EnvironmentKey::JWT_SECRET],['HS512']);
		$response = json_decode(base64_decode($decoded->data), true);
		
		
		$esbResponse = new EsbResponse();
		$esbResponse->revive ($response);
	
		return $esbResponse;
	}
}