<?php 
use EsbCore\Entities\EnvironmentKey;
use Utils\Logger\LogLevel;
use EsbCore\Entities\AppConstants;

/**
 * Sample configuration file, used for testing and as example. 
 * In real life, the producer .env does not contain the consumer keys, and vice versa
 * 
 */

return [
		
	'APP_ENV' => 'local',
		
	'APP_NAME' => 'ESB_CONNECTOR',
		
	'APP_DESCRIPTION' => 'ESB Connector Applicatie',
		
	/*
	|-------------------------------------------------------------------------------
	| The ESB endpoints
	|-------------------------------------------------------------------------------
    |
	*/
		
	EnvironmentKey::ESB_PROXY_ENDPOINT => 'http://esb.localhost/CCHDGEBKRUUFQAPUBNXJRSH',
		
	EnvironmentKey::ESB_PRODUCER_ENDPOINT => 'http://esb.localhost/HRGRPPPQCLKLPLDLCMAVVBT',
		
	EnvironmentKey::ESB_CONSUMER_ENDPOINT => 'http://esb.localhost/XUWNVCKCUVKQHIEJTKAGXMB',

	/*
	 |-------------------------------------------------------------------------------
	 | The keys used by the producer,  copied from the ESB .env and ESB configuration
	 |-------------------------------------------------------------------------------
	 |
	 */	
	
	EnvironmentKey::PRODUCER_KEY => 'TEST_PRODUCER',
	
	EnvironmentKey::PRODUCER_TOKEN => '9f925c11-0a75-4c42-ad2e-2d11599a110a',
	
	
	/*
	 |------------------------------------------------------------------------------
	 | The keys used by the consumer, copied from the ESB .env and ESB configuration
	 |------------------------------------------------------------------------------
	 |
	 */
	
	
	EnvironmentKey::CONSUMER_KEY => 'TEST_CONSUMER',
	
	EnvironmentKey::CONSUMER_TOKEN => '981a6aab-ac4b-492b-8078-a9f3857afec7',
	
	
	/*
	 |--------------------------------------------------------------------------------------------
	 | The keys used by the ESB Pipeline and the producers and consumers, copied from the ESB .env 
	 |--------------------------------------------------------------------------------------------
	 |
	 */
	
	EnvironmentKey::ESB_KEY => 'ESB_PIPELINE',
	
	EnvironmentKey::ESB_TOKEN => '5068f3e7-2cac-4ede-9bb8-5e978545b504',
	
	
	
	
	/*
	 |--------------------------------------------------------------------------
	 | JWT parameters
	 |--------------------------------------------------------------------------
	 |
	 */
	
	EnvironmentKey::JWT_SECRET => 'ABCD',
	
	EnvironmentKey::SERVER_NAME => 'localhost',
	
	/*
	 |------------------------------------------------------------------------------------------------
	 | Message payload RSA encryption.
	 |------------------------------------------------------------------------------------------------
	 |
	 */
	EnvironmentKey::RSA_KEYFILES => 'C:/Users/Aaldert/rsa-keys/',
	
	EnvironmentKey::JWT_RSA_ENCRYPT => AppConstants::DISABLE_RSA_ENCRYPTION,
	
	/*
	 |---------------------------------------------------------------------------------------
	 | Enable localhost Fiddler proxy for cUrl. ENABLE_FIDDLER_PROXY || DISABLE_FIDDLER_PROXY
	 |---------------------------------------------------------------------------------------
	 |
	 */
	EnvironmentKey::FIDDLER_PROXY => CurlHelper::ENABLE_FIDDLER_PROXY,
	
	
	
	/*
	 |--------------------------------------------------------------------------
	 | Query logging
	 |--------------------------------------------------------------------------
	 |
	 */
	
	  'log-query' => AppConstants::ENABLE_QUERY_LOGGING,
	
	  
	 /*
	 |--------------------------------------------------------------------------
	 | Root logger configuration
	 |--------------------------------------------------------------------------
	 |
	 */
	  
	  'log-level' => 'TRACE',
	
	  
	  
	 /*
	  * logging filtering, add exclusion TAG comma-separated format, case insensitive
	  */
	  
	  'log-exclude' => '',
	  
	  'remote_adress' => '127.0.0.1',
	  
	  'log_dir' => 'C:/Users/Aaldert/phplogs/',
	  
	  'DEFAULT_LOGFILE_NAME' => 'esbconnector-info-debug.log',
	  
	  'ERROR_LOGFILE_NAME' => 'esbconnector-error-warn.log',
	  
	  'QUERY_LOGFILE_NAME' => 'esbconnector-query.log'

];

?>