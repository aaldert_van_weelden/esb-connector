<?php
/** \file config.php
* Main configuration script.
* For development configuration see config_local.inc.php
*
* @author Aaldert van Weelden
* @date 23-07-2012
**/


$cfg=array();
$cfg=parse_ini_file(DOC_ROOT.'config.ini');

define('DATETIMEZONE',$cfg['datetimezone']);

define('COPYRIGHT',"&copy; 2011-".date('Y').' <a href="http://www.vanweeldencodeprojects.eu">Van Weelden Code Projects</a>');
define('VERSION',$cfg['application-version']);

date_default_timezone_set ( DATETIMEZONE );

if($cfg['error-reporting']===true){
	error_reporting(E_ALL);
}else{
	error_reporting(0);
}

?>
