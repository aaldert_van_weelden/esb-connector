<?php namespace Stub\Connector;
use Utils\Logger\LoggerInstance;
use EsbCore\Connector\ConsumerConnector;
use \EsbMessage;

class StubConsumerConnector{
	
	
	const TAG = 'StubConsumerConnector';
	private $log;
	
	private $auth;
	private $endpoint;
	private $consumer;
	
	
	/**
	 * The constructor
	 * Insert here the services provided by the IOC container
	 */
	public function __construct(){
		
		$this->log = new LoggerInstance(self::TAG);
	}
	
	/**
	 * Generic ELW consumer
	 * Digest the recieved message here
	 * 
	 * @endpoint /esbconsumer
	 */
	public function digest($payload=null){
		$this->log->debug('digest: recieving message from ESB');
		
		$this->consumer = new ConsumerConnector(function(EsbMessage $message){
			
			$this->log->debug('StubConsumerConnector: sending stubbed response for message['.$message->getID().']');
			
			$response = new \EsbResponse('ESB_PIPELINE');
			$response->setMessageID($message->getID());
			$response->setPayload($payload);
			$response->setSuccess(EsbMessage::SUCCESS);
			$response->setStatus(\EsbMessageStatus::ACCEPTED);
			
			$this->envelope->encode($response);
			$this->envelope->sendJSON();
		
		}, $this->endpoint, $this->auth );
		
		$this->consumer->handleMessage();
		
	}
	
	
}

?>