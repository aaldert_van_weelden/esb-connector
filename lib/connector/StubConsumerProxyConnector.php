<?php namespace Stub\Connector;
use Utils\Logger\LoggerInstance;
use EsbCore\Connector\ConsumerConnector;
use \EsbMessage;

class StubConsumerProxyConnector{
	
	
	const TAG = 'StubConsumerProxyConnector';
	private $log;
	private $consumer;
	
	
	/**
	 * The constructor
	 * Insert here the services provided by the IOC container
	 */
	public function __construct(){
		
		$this->log = new LoggerInstance(self::TAG);
	}
	
	/**
	 * Generic ELW consumer
	 * Digest the recieved message here
	 * 
	 * @endpoint /esbconsumer
	 */
	public function digest(\EsbResponse $response){
		$this->log->debug('digest: recieving message from ESB');
		
		$this->consumer = new ConsumerConnector(function(EsbMessage $message){
			
			$this->log->debug('StubConsumerProxyConnector: sending  response by proxy for message['.$message->getID().']');

			$response->setMessageID($message->getID());
			$this->envelope->encode($response);
			$this->envelope->sendJSON();
		
		}, $this->endpoint, $this->auth );
		
		$this->consumer->handleMessage();
		
	}
	
	
}

?>