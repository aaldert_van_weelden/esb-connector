<?php  namespace Stub\Connector;

use Utils\Logger\LoggerInstance;
use EsbCore\Connector\ProducerPipelineConnector;
use EsbCore\Entities\EnvironmentKey;
use \CurlAuth;
use \EsbMessage;
use \EsbResponse;

/**
 * Producer used to connect to the ESB Pipeline
 * You can use this class in a service for example:
 * <br><br><code>
 * $conn = new EsbProducerConnector();<br>
 * $payload = 'blah';<br>
 * $conn->dispatchMyMessage($payload)
 * </code>
 */
class StubProducerConnector {
	
	const TAG = 'EsbProducerConnector';
	
	private $log;
	private $producer;
	private $endpoint;
	private $auth;
	
	public function __construct() {
		$this->log = new LoggerInstance(self::TAG);
		$this->auth = new \CurlAuth($_ENV[EnvironmentKey::ESB_KEY], $_ENV[EnvironmentKey::ESB_TOKEN]);
	}
	
	/**
	 * Include this method to send a test message to the consumer tot test.
	 */
	public function dispatchTestMessageToConsumer(\EsbMessage $message) {
	

		$this->producer->dispatch ( $message, function (EsbResponse $response) use ($message) {
			//The response from the consumer
			$this->log->dump($response);
			$response->sendJSON();
		} );
	}
	
	public function init($endpoint){
		$this->endpoint = $endpoint;
		$this->producer = new ProducerPipelineConnector ( $this->endpoint, $this->auth );
	}
	
	
}

?>