<?php
require_once realpath(__DIR__.'/../..').'/application.php';
require_once 'commands/ConsumerCommand.php';
require_once 'commands/ConsumerCommand.php';
use Utils\Logger\LoggerInstance;
use EsbCore\Entities\EnvironmentKey;
use EsbCore\Connector\EsbConsumer;
use XHR\HTTPcodes;
use EsbCore\Connector\DTO\AuthDTO;
use EsbCore\Connector\ConsumerStatus\ElwMessageStatus;


/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 * Testing the ELW user messages consuming
 */

class ElwConsumer extends EsbConsumer{
	
	const TAG = 'ElwConsumer';
	
	/*
	 * TEST FLAGS
	 */
	const KEY = 'ELW_CONSUMER';//normally in consumer .env file
	const TOKEN = 'F0BC5869-3C13-46B0-A84F-452649A41FA3';//normally in consumer .env file
	
	private $log;
	private $messageStatus;
	
	public function __construct(){
		
		$this->log = new LoggerInstance(self::TAG);
		$this->message = new EsbMessage();	
		$this->EsbConsumerEndpoint = $_ENV[EnvironmentKey::ESB_CONSUMER_ENDPOINT];
		$this->auth = new CurlAuth(self::KEY, self::TOKEN);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		$this->log->debug('Trying to send the ELW consumer status to the ESB for message['.$this->message->getID().']');
		
		$message = new EsbMessage('ElwMessageStatus', 'The producer prescribed ELW consumerstatus set by the ELW_consumer test application', self::KEY);
		$message->setStatus($this->messageStatus);
		$message->setPayload(new AuthDTO( (object)['key'=>self::KEY, 'token'=>self::TOKEN]));
		
		$uri = $this->EsbConsumerEndpoint.'/setStatus/'.$this->message->getID();
		
		$connector = new CurlHelper(CurlMethod::POST,$uri, $message, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		//$response = $connector->getResponse();
		$response = $connector->getResponseObject();
		
		$this->log->dump($response);
		
		return $response;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(){
		
		//TODO use authentication service
		if( !(isset($_SERVER['PHP_AUTH_USER']) && 
					$_SERVER['PHP_AUTH_USER']===$_ENV[EnvironmentKey::ESB_KEY] && 
					$_SERVER['PHP_AUTH_PW']===$_ENV[EnvironmentKey::ESB_TOKEN]) )
		{
			ExceptionHelper::sendExceptionJsonResponse(new AuthenticationException(self::KEY.': Acces not allowed, wrong credentials'));
		}
		
		switch($_SERVER['REQUEST_METHOD']){
			case 'GET':
				//getStatus request
				$this->response = new EsbResponse(self::KEY.'_TESTRESPONSE');
				$this->response->setStatus(new ElwMessageStatus(ElwMessageStatus::CANCELLED) );
				$this->sendResponse(HTTPcodes::HTTP_OK);
				
				break;
				
			case 'POST':
				$this->response = new EsbResponse(self::KEY.'_TESTRESPONSE');
				
				try {
					$this->message->revive( json_decode(file_get_contents("php://input"), true) );
				
					$this->log->dump( $this->message );
					
					//process message here further by commander pattern
					$command = new ConsumerCommand(function(CommandCallbackResult $result){

						switch($result->getSender()){
							case ConsumerCommander::PARSE_PRODUCER_MESSAGE:
								
								$this->response->setMessageID($this->message->getID());

								if($result->isSuccess()){
									
									//return the consumer status to the ESB. In real life the status is determined by a consumer application service
									$this->messageStatus = new ElwMessageStatus(ElwMessageStatus::BUSY);
									$this->messageStatus->translate();
									$this->setStatus();
									
									$this->response->setStatus(new EsbResponseStatus(EsbResponseStatus::HTTP_ACCEPTED) );
									$this->response->getStatus()->translate();
									$this->sendResponse(HTTPcodes::HTTP_OK);
								}else{
									$this->response->setStatus(new EsbResponseStatus(EsbResponseStatus::HTTP_INTERNAL_SERVER_ERROR) );
									$this->response->setSuccess(EsbResponse::FAILURE);
									$this->response->setError('ELW Test Application failed to handle request correctly');
									$this->sendResponse(HTTPcodes::HTTP_INTERNAL_SERVER_ERROR);
								}
								break;
						}	
					});
					
					$command->run( $this->message );
		
						
				} catch (Exception $e) {
					ExceptionHelper::sendExceptionJsonResponse(new ParseException('Message has wrong format: '.$e->getMessage()));
				}
				break;
				//illegal mehods, only POST and GET are allowed
			default:
				ExceptionHelper::sendExceptionJsonResponse(new BadRequestException('Wrong method: HTTP_'.$_SERVER['REQUEST_METHOD'].' , access not allowed'));
				break;
		}
	}
}

/********************************** ELW TEST CONSUMER APPLICATION ******************************************
 * 
 */

ini_set('html_errors', false);

$consumer = new ElwConsumer();

$consumer->handleMessage();


?>