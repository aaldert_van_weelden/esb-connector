<?php
require_once realpath(__DIR__.'/../..').'/application.php';
require_once 'data/DataGenerator.php';

use Utils\Logger\LoggerInstance;
use Utils\Util;
use EsbCore\Connector\EsbProducer;
use EsbCore\Entities\EnvironmentKey;
use EsbCore\Connector\ConsumerStatus\GrippaMessageStatus;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use EsbCore\Connector\ConsumerStatus\ElwMessageStatus;

/**
 * Producer dummy application, used to send test messages to the ESB Pipeline
 */

class ElwProducerConnector extends EsbProducer{
	
	const TAG = 'ElwProducerConnector';
	
	const KEY = 'ELW_AUTH_REQUEST';//in real life this is retrieved from the application .env file
	const TOKEN = '4B1E4D8A-F49D-4EC1-8805-5DA9EE4EF41B';//in real life this is retrieved from the application .env file
	
	private $log;
	
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		
		$this->EsbProducerProxyEndpoint = $_ENV[EnvironmentKey::ESB_PROXY_ENDPOINT];
		$this->auth = new CurlAuth(self::KEY, self::TOKEN);
	}
	
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch(\EsbMessage $message, callable $callback=null){
		print __FUNCTION__ . ":\n";
		$this->log->debug('ElwProducerConnector::dispatch Trying to send the test message with id['.$message->getID().']');
		
		$jwtMessage = new JwtRequest();
		$encoded = $jwtMessage->encodeAndEncrypt($message);
		
		$this->log->dump($jwtMessage, 'JWT encoded message');
		
		$connector = new CurlHelper(CurlMethod::POST,$this->EsbProducerProxyEndpoint, $jwtMessage, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		$this->log->dump($connector->getResponse(), 'The Portaal authentication response');
		
		$jwtResponse = new JwtResponse();
		$portalResponse = $jwtResponse->decode($connector->getResponse());
		
		return $portalResponse;
		
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){
		
	}
	
	
	/**
	 * Create a testmessage
	 */
	public function createMessage(){
		print __FUNCTION__ . ":\n";
		$this->log->debug('ElwProducerConnector::createMessage  Creating the test message');

		$dto = DataGenerator::getElwPortaalUser(true);//testdata, normally this is provided by the Producer application
		$message = new EsbMessage('ELW Portaal user', 'ELW Portaaluser instance', $this->auth->applicationKey);
		$message->setStatus(DataGenerator::getTestStatus());
		Util::dump($dto,'The Portaal user  test DTO');
		
		$message->setPayload($dto);
		
		return $message;
			
	}
	
	
}

?>