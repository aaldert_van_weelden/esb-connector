<?php
require_once 'ConsumerCommander.php';
use \Utils\Logger\LoggerInstance as LoggerInstance;

/**
 * Use this pattern to handle the message in the consumer application
 * @author Aaldert
 *
 */
class ConsumerCommand implements Commands{
	
	const TAG = 'ConsumerCommand';
	
	private $log;
	private $callback;
	private $commander;
	
	
	public function __construct($callback){
		$this->log = new LoggerInstance(self::TAG);
		$this->callback = $callback;
		$this->commander = new ConsumerCommander();
	}
	
	/**
	 * Execute the commands
	 */
	public function run($data = null){
			
		try{
			$this->commander->execute(ConsumerCommander::PARSE_PRODUCER_MESSAGE, array($this,'onExecuted'), $data);
		}catch(Exception $e){
			$message="A fatal error has occured while processing the message";
			$this->log->error($message,$e->getMessage());
		}
	}
	
	public function onExecuted($result){
	
		switch($result->getSender()){
	
			case ConsumerCommander::PARSE_PRODUCER_MESSAGE:
				$this->log->trace(self::TAG." recieved PARSE_PRODUCER_MESSAGE result in callback");
				
				//$this->log->dump($result, 'PARSE_PRODUCER_MESSAGE');
				
				if($result->isSuccess()){
					$this->log->debug($result->getSender().$result->getInfo()->format().": ".$result->getDescription());
				}else{
					$this->log->error("executed with failure ".$result->getSender(),$result->getError());
	
				}
				call_user_func($this->callback, $result);
				break;
				
			default:
				$this->log->error("no sender provided, returning <null>",$result->getError());
				return null;
	
	
		}
	
	}
	
	/**
	 * Send the collected reports to the admin by email
	 */
	private function sendReport(){
		//TODO implement sending reports by email
	}
}



?>