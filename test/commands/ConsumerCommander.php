<?php

use Utils\Logger\LoggerInstance as LoggerInstance;
use EsbCore\Connector\ConsumerStatus\ElwMessageStatus;
/**
 * commander, use this to track and execute commands
 *
 *  @author Aaldert van Weelden
 */
class ConsumerCommander extends Commander{
 
	const TAG = 'ConsumerCommander';
	
	//The commands
	const PARSE_PRODUCER_MESSAGE = "parse.producer.message";
	
	private $log;
	private $result;
	private $initializedMessages;
	private $infoReport;
	private $errorReport;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		$this->infoReport = new \CommandReport(\CommandReport::INFO);
		$this->errorReport = new \CommandReport(\CommandReport::ERROR);
	}
	
	/**
	 * execute
	 */
	public function execute($command=null,$callback=array(), $message=null, $token=null){
		$this->log->debug("trying to execute command ".$command);
		
		switch($command){
			
			case self::PARSE_PRODUCER_MESSAGE :
				
				$this->log->info ( "execute " . self::PARSE_PRODUCER_MESSAGE );
				$result = new CommandCallbackResult ();
				
				//handle the message here, set the cunsumer status and propagate the error report to the next level
				$this->log->dump($message,'The message to process');
				$this->infoReport->appendEOL('Handling the message here and send response to the ESB Pipeline');
				
				
				$result->setData ( 'some data to return to the application'  );
				$result->setSender ( self::PARSE_PRODUCER_MESSAGE );
				$result->setSuccess ( CommandCallbackResult::SUCCESS );
				$result->setMessage ( 'A message'  );
				$result->setInfo($this->infoReport);
				$result->setError ( $this->errorReport );
				
				call_user_func ( $callback, $result );
				break;
									
			default:
				$this->log->error("no known command provided, returning <null>");
				return null;
				break;
			
			
		}
	}
	
}
?>