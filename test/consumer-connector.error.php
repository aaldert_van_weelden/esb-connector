<?php
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'commands/ConsumerCommand.php';
require_once 'commands/ConsumerCommand.php';
use Utils\Logger\LoggerInstance;
use EsbCore\Connector\ConsumerStatus\GrippaMessageStatus;
use EsbCore\Connector\EsbConsumer;
use EsbCore\Entities\EnvironmentKey;
use XHR\HTTPcodes;


/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class ConsumerError extends EsbConsumer{
	
	const TAG = 'ConsumerError';
	
	/*
	 * TEST FLAGS
	 */
	const FAILURE_TEST = true;
	const KEY = 'ERROR_CONSUMER';//get this from .env file in real life
	const TOKEN = 'A8D2605F-C908-48A9-95CB-5AAD00F721F5';//get this from .env file in real life
	
	private $log;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		
	}
	
	/**
	 * Send a testmessage
	 */
	public function sendMessage(){
		$this->log->debug('Consumer::sendMessage  Trying to send the test message');
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(callable $callback=null){
		
		//TODO use authentication service
		if( !(isset($_SERVER['PHP_AUTH_USER']) && 
					$_SERVER['PHP_AUTH_USER']===$_ENV[EnvironmentKey::ESB_KEY] && 
					$_SERVER['PHP_AUTH_PW']===$_ENV[EnvironmentKey::ESB_TOKEN]) )
		{
			ExceptionHelper::sendExceptionJsonResponse(new AuthenticationException(self::KEY.': Acces not allowed, wrong credentials'));
		}
		
		switch($_SERVER['REQUEST_METHOD']){
			case 'GET':
				//getStatus request
				$this->response = new EsbResponse(self::KEY);
				$this->response->setStatus(new GrippaMessageStatus(GrippaMessageStatus::CLOSED) );
				$this->sendResponse(HTTPcodes::HTTP_OK);
				
				break;
				
			case 'POST':
				$this->response = new EsbResponse(self::KEY);
				
				try {
					$message = new EsbMessage();
					$message->revive( json_decode(file_get_contents("php://input"), true) );
				
					$this->log->dump( $message );
					
					//process message here further by commander pattern
					$command = new ConsumerCommand(function(CommandCallbackResult $result) use($message){

						switch($result->getSender()){
							case ConsumerCommander::PARSE_PRODUCER_MESSAGE:
								
								$this->response->setMessageID($message->getID());
								
								if(self::FAILURE_TEST) $result->setSuccess(false);
								
								if($result->isSuccess()){
										
									$this->response->setStatus(new EsbResponseStatus(EsbResponseStatus::HTTP_ACCEPTED) );
									$this->sendResponse(HTTPcodes::HTTP_OK);
								}else{
									$this->response->setStatus(new EsbResponseStatus(EsbResponseStatus::HTTP_INTERNAL_SERVER_ERROR) );
									$this->response->setSuccess(EsbResponse::FAILURE);
									$this->response->setError('Application failed to handle request correctly');
									$this->sendResponse(HTTPcodes::HTTP_INTERNAL_SERVER_ERROR);
								}
								break;
						}	
					});
					
					$command->run( $message );
		
						
				} catch (Exception $e) {
					ExceptionHelper::sendExceptionJsonResponse(new ParseException('Message has wrong format: '.$e->getMessage()));
				}
				break;
				//illegal mehods, only POST and GET are allowed
			default:
				ExceptionHelper::sendExceptionJsonResponse(new BadRequestException('Wrong method: HTTP_'.$_SERVER['REQUEST_METHOD'].' , access not allowed'));
				break;
		}
	}
}

/********************************** TEST CONSUMER ERROR RESPONSE APPLICATION ******************************************
 * 
 */

ini_set('html_errors', false);

$consumer = new ConsumerError();

$consumer->handleMessage();

?>