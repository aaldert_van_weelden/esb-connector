<?php
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'commands/ConsumerCommand.php';
require_once 'commands/ConsumerCommand.php';
use Utils\Logger\LoggerInstance;
use EsbCore\Connector\ConsumerStatus\GrippaMessageStatus;
use EsbCore\Connector\EsbConsumer;
use EsbCore\Entities\EnvironmentKey;
use XHR\HTTPcodes;


/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class Consumer extends EsbConsumer{
	
	const TAG = 'Consumer';
	
	/*
	 * TEST FLAGS
	 */
	const FAILURE_TEST = false;
	
	private $log;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);	
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(callable $callback=null){
		
		//TODO use authentication service
		if( !(isset($_SERVER['PHP_AUTH_USER']) && 
					$_SERVER['PHP_AUTH_USER']===$_ENV[EnvironmentKey::ESB_KEY] && 
					$_SERVER['PHP_AUTH_PW']===$_ENV[EnvironmentKey::ESB_TOKEN]) )
		{
			ExceptionHelper::sendExceptionJsonResponse(new AuthenticationException(self::TAG.': Acces not allowed, wrong credentials'));
		}
		
		switch($_SERVER['REQUEST_METHOD']){
			case 'GET':
				//getStatus request
				$this->response = new EsbResponse($_ENV[EnvironmentKey::CONSUMER_KEY]);
				$this->response->setStatus(new GrippaMessageStatus(GrippaMessageStatus::CLOSED) );
				$this->sendResponse(HTTPcodes::HTTP_OK);
				
				break;
				
			case 'POST':
				$this->log->trace('Consumer: trying to digest the encoded ESB_PIPELINE message');
				$this->log->dump(file_get_contents("php://input"), 'Consumer: php://input');
				
				try {
					
					$jwtMessage = new JwtRequest();
					$this->message = $jwtMessage->decodeAndDecrypt( file_get_contents ( "php://input" ) );
						
					$this->log->dump ( $this->message , 'The revived producermessage as digested by the consumer');

					//process message here further by commander pattern
					$command = new ConsumerCommand(function(CommandCallbackResult $result){

						switch($result->getSender()){
							case ConsumerCommander::PARSE_PRODUCER_MESSAGE:
								
								//send the response back to the ESB_PIPELINE after signing and encryption
								$this->response = new EsbResponse($_ENV[EnvironmentKey::CONSUMER_KEY]);
								$this->response->setMessageID($this->message->getID());
								
								if(self::FAILURE_TEST) $result->setSuccess(false);
								
								if($result->isSuccess()){
									$this->response->setPayload($result->getInfo()->format());
									$this->response->setStatus(new EsbResponseStatus(EsbResponseStatus::HTTP_ACCEPTED) );
									$jwtResponse = new JwtResponse();
									$jwtResponse->encode($this->response);
									
									$jwtResponse->sendJSON();
								}else{
									ExceptionHelper::sendExceptionJsonResponse(  new InvalidStatusException(  $result->getError()->format() ) );
								}
								break;
						}	
					});
					
					$command->run( $this->message );
		
						
				} catch (Exception $e) {
					ExceptionHelper::sendExceptionJsonResponse(new ParseException('Message has wrong format: '.$e->getMessage()));
				}
				break;
				//illegal mehods, only POST and GET are allowed
			default:
				ExceptionHelper::sendExceptionJsonResponse(new BadRequestException('Wrong method: HTTP_'.$_SERVER['REQUEST_METHOD'].' , access not allowed'));
				break;
		}
	}
}

/********************************** TEST CONSUMER APPLICATION ******************************************
 * 
 */

ini_set('html_errors', false);

$consumer = new Consumer();

$consumer->handleMessage();

?>