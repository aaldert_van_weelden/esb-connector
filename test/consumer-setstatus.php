<?php
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'commands/ConsumerCommand.php';
require_once 'commands/ConsumerCommand.php';
use Utils\Logger\LoggerInstance;
use EsbCore\Connector\ConsumerStatus\GrippaMessageStatus;
use EsbCore\Connector\EsbConsumer;
use EsbCore\Entities\EnvironmentKey;
use XHR\HTTPcodes;
use EsbCore\Connector\DTO\AuthDTO;


/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class ConsumerStatus extends EsbConsumer{
	
	const TAG = 'ConsumerStatus';
	
	/*
	 * TEST FLAGS
	 */
	const FAILURE_TEST = false;
	const KEY = 'STATUS_CONSUMER';////get this from .env file in real life
	const TOKEN = 'b6f61618-cccc-40b1-b3f5-76a4ef392c24';////get this from .env file in real life
	
	private $log;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		$this->message = new EsbMessage();
		$this->EsbConsumerEndpoint = $_ENV[EnvironmentKey::ESB_CONSUMER_ENDPOINT];
		$this->auth = new CurlAuth(self::KEY, self::TOKEN);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		$this->log->debug('Trying to send the consumer status to the ESB for message['.$this->message->getID().']');
		
		$message = new EsbMessage('GrippaMessageStatus', 'The producer prescribed consumerstatus set by the TEST_consumer application', self::KEY);
		$message->setStatus(new GrippaMessageStatus( GrippaMessageStatus::CLOSED));
		$message->setPayload(new AuthDTO( (object)['key'=>self::KEY, 'token'=>self::TOKEN]));
		
		$uri = $this->EsbConsumerEndpoint.'/setStatus/'.$this->message->getID();
		
		$connector = new CurlHelper(CurlMethod::POST,$uri, $message, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		//$response = $connector->getResponse();
		$response = $connector->getResponseObject();
		
		return $response;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(callable $callback=null){
		
		//TODO use authentication service
		if( !(isset($_SERVER['PHP_AUTH_USER']) && 
					$_SERVER['PHP_AUTH_USER']===$_ENV[EnvironmentKey::ESB_KEY] && 
					$_SERVER['PHP_AUTH_PW']===$_ENV[EnvironmentKey::ESB_TOKEN]) )
		{
			ExceptionHelper::sendExceptionJsonResponse(new AuthenticationException(self::KEY.': Acces not allowed, wrong credentials'));
		}
		
		switch($_SERVER['REQUEST_METHOD']){
			case 'GET':
				//getStatus request
				$this->response = new EsbResponse(self::KEY);
				$this->response->setStatus(new GrippaMessageStatus(GrippaMessageStatus::CLOSED) );
				$this->sendResponse(HTTPcodes::HTTP_OK);
				
				break;
				
			case 'POST':
				$this->response = new EsbResponse(self::KEY);
				
				try {
					$this->message->revive( json_decode(file_get_contents("php://input"), true) );
				
					$this->log->dump( $this->message );
					
					//process message here further by commander pattern
					$command = new ConsumerCommand(function(CommandCallbackResult $result){

						switch($result->getSender()){
							case ConsumerCommander::PARSE_PRODUCER_MESSAGE:
								
								$this->response->setMessageID($this->message->getID());
								
								if(self::FAILURE_TEST) $result->setSuccess(false);
								
								if($result->isSuccess()){
										
									//return the consumer status to the ESB
									$this->setStatus();
									$this->response->setStatus(new EsbResponseStatus(EsbResponseStatus::HTTP_ACCEPTED) );
									$this->sendResponse(HTTPcodes::HTTP_OK);
								}else{
									$this->response->setStatus(new EsbResponseStatus(EsbResponseStatus::HTTP_INTERNAL_SERVER_ERROR) );
									$this->response->setSuccess(EsbResponse::FAILURE);
									$this->response->setError('Application failed to handle request correctly');
									$this->sendResponse(HTTPcodes::HTTP_INTERNAL_SERVER_ERROR);
								}
								break;
						}	
					});
					
					$command->run( $this->message );
		
						
				} catch (Exception $e) {
					ExceptionHelper::sendExceptionJsonResponse(new ParseException('Message has wrong format: '.$e->getMessage()));
				}
				break;
				//illegal mehods, only POST and GET are allowed
			default:
				ExceptionHelper::sendExceptionJsonResponse(new BadRequestException('Wrong method: HTTP_'.$_SERVER['REQUEST_METHOD'].' , access not allowed'));
				break;
		}
	}
}

/********************************** TEST CONSUMER SET STATUS APPLICATION ******************************************
 * 
 */

ini_set('html_errors', false);

$consumer = new ConsumerStatus();

$consumer->handleMessage();


?>