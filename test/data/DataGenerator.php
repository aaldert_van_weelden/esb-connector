<?php
require_once 'TEST_USER.php';
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;

use EsbCore\Entities\EnvironmentKey;
use Utils\DateTimeUtil;
use Esb\Connector\Payload\TEST_USER;
/**
 * class used to generate testdata
 */

class DataGenerator{
	

	const ID = 'abcdefgh-f779-428c-accc-d67f1050441b';
	const NAME = 'testname';
	const DESCRIPTION = 'testdescription';
	const SENDER = 'testsender';
	const MESSAGE_KEY = 'test.message.key';
	
	public static function getTestMessage(){
		
		$dto = self::getTestUser();
		$message = new EsbMessage('TestUser', 'A TEST_USER instance',$_ENV[EnvironmentKey::PRODUCER_KEY]);
		$message->setID(self::ID);
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
		$message->setPayload($dto);
		return $message;
	}
	
	public static function getTestMessageNoClass(){
	
		$dto = self::getTestUser();
		unset($dto->clazz);
		$message = new EsbMessage('TestUser', 'A TEST_USER instance',$_ENV[EnvironmentKey::PRODUCER_KEY]);
		
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
		$message->setPayload($dto);
		return $message;
	}
	
	public static function getTestUser(){
		$test = new TEST_USER();
		
		$test->ref_id = 'JDRU';//unique identifier key
		$test->voornaam = 'Jan';
		$test->achternaam = 'van Drunen';
		$test->username = 'jand';
		$test->email = 'jan@drunen.nl';
		
		return $test;
	}
	
	public static function getTestStatus(){
		return new ProducerMessageStatus(ProducerMessageStatus::TEST);
	}
	
	/**
	 * Simulate the result as returned by the Eloquent Message::find('�d')->first() method
	 * @return stdClass
	 */
	public static function getTestMessageModel(){
		
		$entity = new stdClass;
		
		$entity->id 			= null;
		$entity->clazz 			= 'Message';
		$entity->message_id  	= self::ID;
		$entity->name 			= self::NAME;
		$entity->description 	= self::DESCRIPTION;
		$entity->sender 		= self::SENDER;
		$entity->status 		= \PersistHelper::parse2JSON_encodedString(self::getTestStatus());
		$entity->payload 		= \PersistHelper::parse2JSON_encodedString(self::getTestUser());
		$entity->success 		= 1;
		$entity->error 			= 0;
		$entity->created_at		= DateTimeUtil::getNow();
		$entity->updated_at 	= DateTimeUtil::getNow();
		
		return $entity;
	}
	
}

?>