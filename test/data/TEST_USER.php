<?php namespace  Esb\Connector\Payload;

/**
 * The class used to wrap the payload to use for a specific message and to define the additional payload properties
 * As a convention use the APPNAME_ENTITYNAME format 
 * @author Aaldert van Weelden
 *
 */
class TEST_USER extends \EsbPayload {

	/**
	 * The payload class as key, can be used to identify a specific service method to call in the clent application
	 * If set, the class is revived in the responses
	 * @var string
	 */
	public $clazz = 'TEST_USER';//used for response revival 
	public $ref_id;

	public $voornaam;
	public $achternaam;
	public $email;
	public $straat;
	public $postcode;
	public $plaats;
	public $telefoon;
	public $geboortedatum;

	public $username;

	public function __construct(){
		parent::__construct();
	}

	public function map($user){
		$this->ref_id = empty($user->ref_id) ? $user->id : $user->ref_id;

		$this->voornaam =  $user->firstname;
		$this->achternaam =  $user->lastname;
		$this->email = $user->email;
		$this->straat =  $user->adress;
		$this->postcode =  $user->zipcode;
		$this->plaats =  $user->city;
		$this->telefoon =  $user->phone;
		$this->geboortedatum = $user->date_of_birth;

		$this->username = $user->username;

	}
}