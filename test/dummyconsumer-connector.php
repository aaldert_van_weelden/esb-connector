<?php
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'commands/ConsumerCommand.php';
require_once 'commands/ConsumerCommand.php';
use Utils\Logger\LoggerInstance;
use EsbCore\Entities\EnvironmentKey;


/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class DummyConsumer extends EsbConsumer{
	
	const TAG = 'DummyConsumer';
	
	const KEY = 'DUMMY_CONSUMER';//get this from .env file in real life
	const TOKEN = '6059d59b-f779-428c-accc-d67f1050441b';//get this from .env file in real life
	
	/*
	 * TEST FLAGS
	 */
	const FAILURE_TEST = false;
	
	private $log;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		$this->message = new EsbMessage();
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(callable $callback=null){
		
		//TODO use authentication service
		if( !(isset($_SERVER['PHP_AUTH_USER']) && 
					$_SERVER['PHP_AUTH_USER']===$_ENV[EnvironmentKey::ESB_KEY] && 
					$_SERVER['PHP_AUTH_PW']===$_ENV[EnvironmentKey::ESB_TOKEN]) )
		{
			ExceptionHelper::sendExceptionJsonResponse(new AuthenticationException(self::KEY.': Acces not allowed, wrong credentials'));
		}
		
		switch($_SERVER['REQUEST_METHOD']){
			case 'GET':
				
				
				break;
				
			case 'POST':
				$this->response = new EsbResponse(self::KEY);
				
				break;
				//illegal mehods, only POST and GET are allowed
			default:
				ExceptionHelper::sendExceptionJsonResponse(new BadRequestException('Wrong method: HTTP_'.$_SERVER['REQUEST_METHOD'].' , access not allowed'));
				break;
		}
	}
}

/********************************** TEST DUMMYCONSUMER APPLICATION ******************************************
 * 
 */

ini_set('html_errors', false);

$consumer = new DummyConsumer();

$consumer->handleMessage();

?>