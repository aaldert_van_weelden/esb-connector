<?php

require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';
require_once 'generic/esb-producerconnector.php';

use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;

error_reporting(E_ALL);
ini_set('html_errors', false);

/**
 * Test the Payload
 * 
 * @author Aaldert van Weelden
 *
 */
class EsbPayloadTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line('------------------------------------------------');
	}
	

// 	public function testProducerToConsumer(){
// 		Out::print_line(__FUNCTION__);
		
		
// 		$producer = new EsbProducerConnector();
		
// 		$message = DataGenerator::getTestMessageNoClass();
		
// 		Out::dump($message,'The message to send');
// 		$response = $producer->dispatch ( $message, function (EsbResponse $response) use($message) {
			
// 			Out::dump ( $response, 'The ConsumerJwt response in the callback' );
// 			$this->assertNotNull ( $response );
// 			$this->assertTrue ( is_object ( $response ) );

// 			$this->assertEqual ( get_class ( $response ), 'EsbResponse' );
// 			$this->assertNotNull ( $response->getStatus () );
// 			$this->assertEqual ( $response->getStatus (), EsbMessageStatus::ACCEPTED );
// 			 Out::print_line('messageID: '.$message->getID());
// 			$this->assertEqual ( $message->getID(), $response->getMessageID() );
// 		} );
// 	}
	
	public function testPayloadRevival(){
		Out::print_line(__FUNCTION__);
		
		$requestMessage = DataGenerator::getTestMessage();
		
		$payload =  [
				'clazz' => 'TEST_USER',
				'ref_id' => '1234',
				'voornaam' => 'Snarf',
				'achternaam' => 'Olla',
				'email' => 'snarf@test.nl',
				'straat' => 'Teststraat',
				'postcode' => '4567YU',
				'plaats' => 'Dorp',
				'telefoon' => '056584778',
				'geboortedatum' => '11-01-63',
				'username' => 'snarf-01'
		]; 
		
		$requestMessage->setPayload($payload);
		$requestMessage->setStatus(['value'=>'test', 'clazz'=>'EsbCore\Connector\ProducerStatus\ProducerMessageStatus']);
		
		Out::dump ( $requestMessage, 'The request message' );
		
		$request = new EsbMessage();
		
		$request->revive($requestMessage);
		
		
		Out::dump ( $request, 'The revived request message' );
		
	}
		
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new EsbPayloadTest())->run();


?>