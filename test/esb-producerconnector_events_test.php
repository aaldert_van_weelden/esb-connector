<?php

require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';
require_once 'generic/esb-producerconnector.php';

use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use VWIT\Base\Event;
use EsbCore\Connector\ESB;

error_reporting(E_ALL);
ini_set('html_errors', false);

/**
 * Test the ProducerConnector
 * 
 * @author Aaldert van Weelden
 *
 */
class EsbProducerConnectorEventsTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		EsbProducerConnector::bind();
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line('------------------------------------------------');
	}
	

	public function testProducerToConsumerFirstEvent(){
		Out::print_line(__FUNCTION__);
		
		$payload = new EsbPayload();
		$payload->user = 345;
		$payload->courses = [1,2,3,4,5];
		
		Out::dump($payload,'The EVENT_TYPE_FIRST payload to send');
		
		//trigger the event
		
		ESB::fire(EsbProducerConnector::EVENT_TYPE_FIRST, $payload);
	
	}
	
	public function testProducerToConsumerSecondEvent(){
		Out::print_line(__FUNCTION__);
		
		//$producer = EsbProducerConnector::instance();
	
		$payload = new EsbPayload();
		$payload->sender = 'user_import';
		$payload->users= [6,7,8,9];
	
		Out::dump($payload,'The EVENT_TYPE_SECOND message to send');
	
		//trigger the event
	
		ESB::fire(EsbProducerConnector::EVENT_TYPE_SECOND, $payload);
	
	}
		
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new EsbProducerConnectorEventsTest())->run();


?>