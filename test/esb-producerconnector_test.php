<?php

require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';
require_once 'generic/esb-producerconnector.php';

use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use EsbCore\Entities\EnvironmentKey;

error_reporting(E_ALL);
ini_set('html_errors', false);

/**
 * Test the ProducerConnector
 * 
 * @author Aaldert van Weelden
 *
 */
class EsbProducerConnectorTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line('------------------------------------------------');
	}
	

	public function testProducerToConsumer(){
		Out::print_line(__FUNCTION__);
		
		
		$producer = new EsbProducerConnector();
		
		$message = new EsbMessage();
		
		$message->setName(DataGenerator::MESSAGE_KEY);
		$message->setDescription(DataGenerator::DESCRIPTION);
		$message->setSender($producer->getAuth()->applicationKey);
		$message->setPayload([
				'user'=>12,
				'courses'=>[23,45,46,47,48,49,50,55,57,80,81]
		]);
		
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::LOW_PRIORITY));
		
		Out::dump($message,'The message to send');
		$response = $producer->dispatch ( $message, function (EsbResponse $response) use($message) {
			
			Out::dump ( $response, 'The ConsumerJwt response in the callback' );
			$this->assertNotNull ( $response );
			$this->assertTrue ( is_object ( $response ) );

			$this->assertEqual ( get_class ( $response ), 'EsbResponse' );
			$this->assertNotNull ( $response->getStatus () );
			$this->assertEqual ( $response->getStatus (), EsbMessageStatus::ACCEPTED );
			 Out::print_line('messageID: '.$message->getID());
			$this->assertEqual ( $message->getID(), $response->getMessageID() );
		} );
	}
		
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new EsbProducerConnectorTest())->run();


?>