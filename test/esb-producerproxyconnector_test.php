<?php
require_once 'generic/esb-producerproxyconnector.php';
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';

use EsbCore\Entities\EnvironmentKey;
use EsbCore\Connector\ProducerProxyConnector;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;


error_reporting(E_ALL);
ini_set('html_errors', false);

/**
 * Test the ProducerConnector
 * 
 * @author Aaldert van Weelden
 *
 */
class EsbProducerProxyConnectorTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line('------------------------------------------------');
	}
	

	public function testProducerProxyToConsumer(){
		Out::print_line(__FUNCTION__);
		
		
		$producer = new EsbProducerProxyConnector();
		
		$message = new EsbMessage();
		
		$message->setName(DataGenerator::MESSAGE_KEY);
		$message->setDescription(DataGenerator::DESCRIPTION);
		$message->setSender($producer->getAuth()->applicationKey);
		
		$message->setPayload([
				'username'=>'user@test.nl',
				'userpassword'=>'ASDFG'
		]);
		
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::HIGH_PRIORITY));
		
		Out::dump($message,'The message to send by proxy');
		$response = $producer->dispatch ( $message, function ($response) use($message){
			
			Out::dump ( $response, 'The ConsumerJwt response in the callback recieved from the proxy' );
			$this->assertNotNull ( $response );
			$this->assertTrue ( is_object ( $response ) );

			$this->assertEqual ( get_class ( $response ), 'EsbResponse' );

			
			$this->assertNotNull($response->getStatus());
			$this->assertEqual($response->getStatus()->value, EsbResponseStatus::HTTP_ACCEPTED);
			
			$this->assertTrue($response->getPayload()->authorized);
			
			
			Out::print_line('messageID: '.$message->getID());
			$this->assertEqual ( $message->getID(), $response->getMessageID() );
		} );
		
	}
		
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new EsbProducerProxyConnectorTest())->run();


?>