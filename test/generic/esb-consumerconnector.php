<?php
require_once realpath(__DIR__.'/../..').'/application.php';;

use EsbCore\Connector\ConsumerConnector;
use Utils\Logger\LoggerInstance;
use Utils\Logger\Logger;

/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class EsbConsumerConnector{
	
	const TAG = 'EsbConsumerConnector';
	
	private $log;
	private $auth;
	private $endpoint;
	private $consumer;
	
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		
		$this->endpoint = 'http://esb-connector.localhost/generic/esb-consumerconnector.php';
		$this->auth = new CurlAuth('CONSUMERCONNECTOR_TEST', '479D3B63-437B-4B05-AF6E-6130BF3D8114');
	}
	
	
	public function sendResponse(){
		
		$this->consumer = new ConsumerConnector(function(EsbMessage $message){
		
			Logger::log()->dump($message);
		
			$payload = ['message_id'=>$message->getID(),'hallo'=>'aaldert'];
			
			$this->consumer->accept($message, $payload);
		
			//$this->consumer->reject($message, 'some nasty error from here');
		
		
		
		}, $this->endpoint, $this->auth );
		
			$this->consumer->handleMessage();
		
	}
	
	
}

/********************************** TEST CONSUMER APPLICATION ******************************************
 * 
 */

ini_set('html_errors', false);

$conn = new EsbConsumerConnector();

$conn->sendResponse();

?>