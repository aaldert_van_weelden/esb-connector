<?php
require_once realpath(__DIR__.'/../..').'/application.php';

use EsbCore\Connector\ConsumerConnector;
use Utils\Logger\LoggerInstance;
use Utils\Logger\Logger;

/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class EsbConsumerProxyConnector{
	
	const TAG = 'EsbConsumerConnector';
	
	private $log;
	private $auth;
	private $endpoint;
	private $consumer;
	
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		
		$this->endpoint = 'http://esb-connector.localhost/generic/esb-consumerproxyconnector.php';
		$this->auth = new CurlAuth('CONSUMERPROXYCONN_TEST', 'DB35C887-8E20-4436-B6FB-94F8BE62945B');
	}
	
	
	public function sendResponse(){
		
		$this->consumer = new ConsumerConnector(function(EsbMessage $message){
		
			Logger::log()->dump($message);
			
			$data = require_once '../data/bigdata.php';
		
			$payload = ['authorized'=>true, 'data'=> $data];
			//$payload = ['message_id'=>$message->getID(),'credentials'=> $message->getPayload() ,'authorized'=>true];
			
			$this->consumer->accept($message, $payload);
		
			//$this->consumer->reject($message, 'some nasty error from here');
		
		
		
		}, $this->endpoint, $this->auth );
		
			$this->consumer->handleMessage();
		
	}
	
	
}

/********************************** TEST CONSUMER APPLICATION ******************************************
 * 
 */

ini_set('html_errors', false);

$conn = new EsbConsumerProxyConnector();

$conn->sendResponse();

?>