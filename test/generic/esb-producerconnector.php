<?php 
require_once realpath(__DIR__.'/../..').'/application.php';
require_once 'data/DataGenerator.php';

use EsbCore\Connector\ProducerPipelineConnector;
use EsbCore\Entities\EnvironmentKey;
use EsbCore\Connector\ESB;
use Utils\Logger\Logger;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;

class EsbProducerConnector extends ProducerPipelineConnector{
	
	const EVENT_TYPE_FIRST = 'event.type.first';
	const EVENT_TYPE_SECOND = 'event.type.second';
	
	/**
	 * The constructor of the test producer
	 */
	public function __construct(){
		
		$this->EsbProducerEndpoint = $_ENV[EnvironmentKey::ESB_PRODUCER_ENDPOINT];
		$this->auth = new CurlAuth('PRODUCERCONNECTOR_TEST', '6BD40A5E-0915-4C6A-9AB9-D9446253FA9C');
		
		parent::__construct($this->EsbProducerEndpoint, $this->auth);
		
	}
	
	/**
	 * Bind the events to listen for here
	 */
	public static function bind() {
		$self = self::instance ();
		if (self::$bound)
			return $self;
		
		ESB::bind ( self::EVENT_TYPE_FIRST, function (EsbPayload $payload) use ($self) {
			
			$message = new EsbMessage();
			
			$message->setName(EsbProducerConnector::EVENT_TYPE_FIRST);
			$message->setDescription('The description for the first event');
			$message->setSender('PRODUCERCONNECTOR_TEST');
			$message->setPayload($payload);
			
			$message->setStatus(new ProducerMessageStatus(ProducerMessageStatus::HIGH_PRIORITY));
			
			
			$self->dispatch ( $message, function (EsbResponse $response) use ($message, $self) {
				Logger::log ()->dump ( $response );
			} );
		} );
		
		ESB::bind ( self::EVENT_TYPE_SECOND, function (EsbPayload $payload) use ($self) {
			
			$message = new EsbMessage();
				
			$message->setName(EsbProducerConnector::EVENT_TYPE_SECOND);
			$message->setDescription('The description for the second event');
			$message->setSender('PRODUCERCONNECTOR_TEST');
			$message->setPayload($payload);
				
			$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::HIGH_PRIORITY));
			
			$self->dispatch ( $message, function (EsbResponse $response) use ($message, $self) {
				Logger::log ()->dump ( $response );
			} );
		} );
		
		self::$bound = true;
		
		return $self;
	}
}


?>