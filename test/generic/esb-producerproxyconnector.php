<?php 
require_once realpath(__DIR__.'/../..').'/application.php';
require_once 'data/DataGenerator.php';

use EsbCore\Connector\ProducerProxyConnector;
use EsbCore\Entities\EnvironmentKey;

class EsbProducerProxyConnector extends ProducerProxyConnector{
	
	/**
	 * The constructor of the test proxy producer
	 */
	public function __construct(){
	
		$this->EsbProducerEndpoint = $_ENV[EnvironmentKey::ESB_PROXY_ENDPOINT];
		$this->auth = new CurlAuth('PRODUCERPROXYCONN_TEST', '40D7BA2C-2399-4BFE-90E5-7E1312B851A0');
	
		parent::__construct($this->EsbProducerEndpoint, $this->auth);
	
	}
}



?>