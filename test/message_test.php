<?php
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';
require_once 'methods/methods-to-test.php';

ini_set('html_errors', false);

/**
 * Put the EsbMessage class to test here
 * 
 * @author Aaldert van Weelden
 *
 */


class MessageTest extends UnitTestCase {
	
	private $message;
	
	public function __construct(){
		parent::__construct();
		Out::print_underline( dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this) );
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		$this->message = DataGenerator::getPortaalTestMessage();
		$this->assertNotNull($this->message);
		$this->assertEqual($this->message->getStatus(), DataGenerator::getTestStatus());
		$this->assertEqual($this->message->getPayload(), DataGenerator::getElwPortaalUser());
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line(  '------------------------------------------------');
	}
	
	
	
	
	
	
	
	
	
	public function testValidate(){
		Out::print_line(  __FUNCTION__ );
		
		
		Out::dump($this->message, 'The test message');
		
		$report = $this->message->validate(['status','payload',]);
		
		$this->assertNotNull($report);
		
		$this->assertTrue($report->isValid());
		$this->assertTrue($report->warnReport->isEmpty());
		$this->assertTrue($report->errorReport->isEmpty());
		
		Out::dump($report, 'The validation report' );
		
	}
	
	public function testValidateWarnStatusPayload(){
		Out::print_line(  __FUNCTION__ );
	
	
		$this->message->setStatus(null);
		$this->message->setPayload(null);
		
		Out::dump($this->message, 'The test message with empty status and payload');
	
		$report = $this->message->validate(['status','payload',]);
	
		$this->assertNotNull($report);
	
		$this->assertTrue($report->isValid());
		
		$this->assertFalse($report->warnReport->isEmpty());
		$this->assertTrue( strpos($report->warnReport->toString(), 'status')>0);
		$this->assertTrue( strpos($report->warnReport->toString(), 'payload')>0);
		
		$this->assertTrue($report->errorReport->isEmpty());
	
		Out::dump($report, 'The validation report' );
	
	}
	
	
	public function testValidateWarnStatusPayloadNotNull(){
		Out::print_line(  __FUNCTION__ );
	
	
		$this->message->setStatus(null);
	
		Out::dump($this->message, 'The test message with empty status, payload not empty');
	
		$report = $this->message->validate(['status','payload',]);
	
		$this->assertNotNull($report);
	
		$this->assertTrue($report->isValid());
		
		$this->assertFalse($report->warnReport->isEmpty());
		$this->assertTrue( strpos($report->warnReport->toString(), 'status')>0);
		$this->assertFalse( strpos($report->warnReport->toString(), 'payload'));
		
		$this->assertTrue($report->errorReport->isEmpty());
	
		Out::dump($report, 'The validation report' );
	
	}
	
	public function testValidateWarnStatus(){
		Out::print_line(  __FUNCTION__ );
	
	
		$this->message->setStatus(null);
		$this->message->setPayload(null);
	
		Out::dump($this->message, 'The test message with empty status and payload, ignore payload');
	
		$report = $this->message->validate(['status']);
	
		$this->assertNotNull($report);
	
		$this->assertTrue($report->isValid());
		
		$this->assertFalse($report->warnReport->isEmpty());
		$this->assertTrue( strpos($report->warnReport->toString(), 'status')>0);
		$this->assertFalse( strpos($report->warnReport->toString(), 'payload'));
	
		$this->assertTrue($report->errorReport->isEmpty());
	
		Out::dump($report, 'The validation report' );
	
	}
	
	public function testValidateErrorID(){
		Out::print_line(  __FUNCTION__ );
	
	
		$this->message->setID(null);
	
		Out::dump($this->message, 'The test message with empty message_id');
	
		$report = $this->message->validate();
	
		$this->assertNotNull($report);
	
		$this->assertFalse($report->isValid());
		$this->assertTrue($report->warnReport->isEmpty());
	
	
		$this->assertFalse($report->errorReport->isEmpty());
		$this->assertTrue( strpos($report->errorReport->toString(), 'message_id')>0);
		$this->assertFalse( strpos($report->errorReport->toString(), 'name'));
		$this->assertFalse( strpos($report->errorReport->toString(), 'sender'));
	
		Out::dump($report, 'The validation report' );
	
	}
	
	
	public function testValidateErrorName(){
		Out::print_line(  __FUNCTION__ );
	
	
		$this->message->setName(null);
	
		Out::dump($this->message, 'The test message with empty name');
	
		$report = $this->message->validate();
	
		$this->assertNotNull($report);
	
		$this->assertFalse($report->isValid());
		$this->assertTrue($report->warnReport->isEmpty());
		
		
		$this->assertFalse($report->errorReport->isEmpty());
		$this->assertTrue( strpos($report->errorReport->toString(), 'name')>0);
		$this->assertFalse( strpos($report->errorReport->toString(), 'message_id'));
		$this->assertFalse( strpos($report->errorReport->toString(), 'sender'));
	
		Out::dump($report, 'The validation report' );
	
	}
	
	public function testValidateErrorSender(){
		Out::print_line(  __FUNCTION__ );
	
	
		$this->message->setSender(null);
	
		Out::dump($this->message, 'The test message with empty sender');
	
		$report = $this->message->validate();
	
		$this->assertNotNull($report);
		
		$this->assertFalse($report->isValid());
	
		$this->assertFalse($report->errorReport->isEmpty());
		$this->assertTrue( strpos($report->errorReport->toString(), 'sender')>0);
		$this->assertFalse( strpos($report->errorReport->toString(), 'name'));
		$this->assertFalse( strpos($report->errorReport->toString(), 'message_id'));
	
		Out::dump($report, 'The validation report' );
	
	}
	
	public function testMessageToDTOwithRevival(){
		Out::print_line(  __FUNCTION__ );
		
		$model = DataGenerator::getTestMessageModel();
		
		Out::dump($model, 'The Message model to convert' );
		
		$dto = Methods::modelToDTO($model);
		
		Out::dump($dto, 'The resulting DTO' );
		
		$orgmessage = DataGenerator::getPortaalTestMessage();
		Out::dump($orgmessage, 'The original message' );
		
		
		$message = new EsbTestMessage();
		Out::print_line(  'new generated message_id: '.$message->getID() );
		Out::print_line(  'original message_id: '.$orgmessage->getID() );
		
		$message->revive($dto);
		Out::print_line(  'message_id after revival: '.$message->getID() );
		
		Out::dump($message, 'The revived message' );
		
		$this->assertEqual($message, $orgmessage);
	}

	
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new MessageTest())->run();

?>