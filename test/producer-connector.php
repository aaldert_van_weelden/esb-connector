<?php
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';

use Utils\Logger\LoggerInstance;
use EsbCore\Connector\EsbProducer;
use EsbCore\Entities\EnvironmentKey;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use Utils\Logger\Logger;

/**
 * Producer dummy application, used to send test messages to the ESB Pipeline
 */

class ProducerConnector extends EsbProducer{
	
	const TAG = 'Producer';
	
	
	
	private $log;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		
		$this->EsbProducerEndpoint = $_ENV[EnvironmentKey::ESB_PRODUCER_ENDPOINT];
		$this->auth = new CurlAuth($_ENV[EnvironmentKey::PRODUCER_KEY], $_ENV[EnvironmentKey::PRODUCER_TOKEN]);
	}
	
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch(\EsbMessage $message, callable $callback=null){
		Out::print_line(  __FUNCTION__ );
		$this->log->debug('Producer::dispatch Trying to send the test message with id['.$message->getID().']');
		
		$jwtMessage = new JwtRequest();
		$encoded = $jwtMessage->encodeAndEncrypt($message);
		
		$this->log->dump($jwtMessage, 'JWT encoded message');
		
		$connector = new CurlHelper(CurlMethod::POST,$this->EsbProducerEndpoint, $jwtMessage, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		$esbResponse = new \EsbResponse();
		$esbResponse->revive( (array) $connector->getResponseObject());
		
		return $esbResponse;
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){
		Out::print_line(  __FUNCTION__ );
		$this->log->debug('Producer::getConsumerStatus  Trying to retrieve the consumer application message status for id['.$messageID.']');
		
		$message = new EsbMessage(null, null, $this->auth->applicationKey);
		$message->setID($messageID);
		
		$connector = new CurlHelper(CurlMethod::GET,$this->EsbProducerEndpoint, $message, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		$response = $connector->getResponse();
		//$response = $connector->getResponseObject();
		return $response->status;
		
	}
	
	
	/**
	 * Create a testmessage
	 */
	public function createMessage(){
		Out::print_line(  __FUNCTION__ );
		$this->log->debug('Producer::createMessage  Creating the test message');

		$dto = DataGenerator::getTestUser();//testdata, normally this is provided by the Producer application
		$message = new EsbMessage('GrippaLeerling', 'A TestUser instance', $this->auth->applicationKey);
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
		Out::dump($dto,'The Test User');
		
		$message->setPayload($dto);
		
		return $message;
			
	}
	
	
}

/**
 * Sending test message, only if the file is called directly, not as an instantiated class.
 * Matching on filename and url
 * @method GET
 */
similar_text($_SERVER['REQUEST_URI'], __FILE__, $match );

if( $_SERVER['REQUEST_METHOD'] === 'GET' && $match>45 ){
	
	$connect = new ProducerConnector();
	$message = new EsbMessage('TESTMESSAGE','class ProducerConnector', $_ENV[EnvironmentKey::PRODUCER_KEY]);
	$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
	
	$message->setPayload('hello world from ProducerConnector @ '.__FILE__);
	$response = $connect->dispatch($message);
	var_dump($response);
}

?>