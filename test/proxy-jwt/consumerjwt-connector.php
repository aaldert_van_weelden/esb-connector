<?php
require_once realpath(__DIR__.'/../..').'/application.php';

use Utils\Logger\LoggerInstance;
use EsbCore\Connector\EsbConsumer;
use XHR\HTTPcodes;
use XHR\JSON;
use Firebase\JWT\JWT;


/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class ConsumerJwt extends EsbConsumer{
	
	const TAG = 'ConsumerJwt';
	
	const SECRET = 'ABCD';
	
	/*
	 * TEST FLAGS
	 */
	const FAILURE_TEST = false;
	
	private $log;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		$this->message = new EsbMessage();
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(callable $callback=null) {
		$this->log->dump ( file_get_contents ( "php://input" ) , '"ConsumerJWT: php://input dump');
		$this->log->trace('handleMessage:: trying to authenticate the user');
		
		try {
			$jwtMessage = new JwtRequest();
			$this->message = $jwtMessage->decodeAndDecrypt( file_get_contents ( "php://input" ) );
			
			$this->log->dump ( $this->message , 'The revived producermessage as digested by the consumer');
			
			//in real life digest and process the producermessage here and prepare a response to the producer

			$this->response = new EsbResponse ( 'JWT Consumer' );
			
			$data = require_once '../data/bigdata.php';
			$this->response->setMessageID ( $this->message->getID () );
			
			$this->response->setPayload( (object) ['authorized'=>true, 'message'=> 'hello from '.__FILE__ , 'course' => $data]);
			//$this->response->setPayload( (object) ['authorized'=>true]);
			
			$this->response->setStatus ( new EsbResponseStatus ( EsbResponseStatus::HTTP_ACCEPTED ) );
			
			$jwtResponse = new JwtResponse();
			$jwtResponse->encode($this->response);
			$jwtResponse->sendJSON();
			
		} catch ( Exception $e ) {
			ExceptionHelper::sendExceptionJsonResponse ( new ParseException ( 'ConsumerJWT: Message has wrong format: ' . $e->getMessage () ) );
		}
	}
}

/********************************** TEST CONSUMER APPLICATION ******************************************
 * 
 */

$consumer = new ConsumerJwt();
$consumer->handleMessage();
?>