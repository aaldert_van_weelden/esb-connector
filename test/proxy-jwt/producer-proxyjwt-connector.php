<?php
require_once realpath(__DIR__.'/../..').'/application.php';
require_once 'data/DataGenerator.php';

use Utils\Logger\LoggerInstance;
use Utils\Util;
use EsbCore\Connector\EsbProducer;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use Firebase\JWT\JWT;
use EsbCore\Entities\EnvironmentKey;

/**
 * Producer dummy application, used to send test messages to the ESB Pipeline
 */

class ProducerProxyJwtConnector extends EsbProducer{
	
	const TAG = 'JWT Producer';
	
	const KEY = 'PRODUCER';
	const TOKEN = '1234';
	
	const SECRET = 'ABCD';
	
	private $log;
	private $proxyEndpoint;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		
		$this->proxyEndpoint = 'http://esb-connector.localhost/proxy-jwt/proxy.php';
		$this->auth = new CurlAuth(self::KEY, self::TOKEN);
	}
	
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch(\EsbMessage $message, callable $callback=null){
		Out::print_line(  __FUNCTION__ );
		$this->log->debug('JWT Producer::dispatch Trying to send the test message with id['.$message->getID().']');
		
		$jwtMessage = new JwtRequest();
		$encoded = $jwtMessage->encodeAndEncrypt($message);
		$this->log->dump($encoded, 'Encoded and encrypted');
		
		$this->log->dump($jwtMessage, 'JwtRequest');
	
		$connector = new CurlHelper(CurlMethod::POST,$this->proxyEndpoint, $jwtMessage, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();//make the call
		
		$this->log->dump ($connector->getResponse() , 'Consumer JWT raw response string');
		
		$response = new JwtResponse();
		$consumerResponse = $response->decode($connector->getResponse());
		
		$this->log->dump ($consumerResponse , 'The consumer response');
		
		return $consumerResponse;
		
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){

	}
	
	
	/**
	 * Create a testmessage
	 */
	public function createMessage(){
		Out::print_line(  __FUNCTION__ );
		$this->log->debug('Producer::createMessage  Creating the test message');

		$dto = DataGenerator::getElwPortaalUser();//testdata, normally this is provided by the Producer application
		$message = new EsbMessage('PortaalUser', 'A Portaal User DTO instance', $this->auth->applicationKey);
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
		Util::dump($dto,'The PortaalUser test DTO');
		
		$message->setPayload($dto);
		
		return $message;
			
	}
	
	
}

?>