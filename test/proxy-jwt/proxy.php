<?php
require_once realpath(__DIR__.'/../..').'/application.php';


use Utils\Logger\LoggerInstance;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use Firebase\JWT\JWT;
use EsbCore\Entities\EnvironmentKey;

/**
 * Producer dummy application, used to send test messages to the ESB Pipeline
 */

class ProxyJwt{

	const TAG = 'ProxyJwt';

	const KEY = 'PROXY';
	const TOKEN = 'ABCD';
	
	const SECRET = 'ABCD';

	private $log;
	private $auth;
	private $consumerEndpoint;
	private $response;

	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);

		$this->consumerEndpoint = 'http://esb-connector.localhost/proxy-jwt/consumerjwt-connector.php';
		$this->auth = new CurlAuth(self::KEY, self::TOKEN);
		
		$this->log->trace('Initializing the Proxy');
	}



	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch(\EsbMessage $message, $callback=null){
		print __FUNCTION__ . ":\n";
		$this->log->debug('ProducerJWT::dispatch Trying to send the test message with id['.$message->getID().']');
		
		$jwtMessage = new JwtRequest();
		$encoded = $jwtMessage->encodeAndEncrypt($message);
		
		$connector = new CurlHelper(CurlMethod::POST, $this->consumerEndpoint, $jwtMessage, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();

		$consumerResponse = $connector->getResponseObject();

		return call_user_func($callback,$consumerResponse);

	}
	
	
	public function handleMessage(callable $callback=null) {
		$this->log->trace('handleMessage:: trying to handle the message and propagate it to the consumer['.$this->consumerEndpoint.']');
		$this->log->dump ( file_get_contents ( "php://input" ) , 'ProxyJwt: php://input dump');

		try {
			
			$jwtMessage = new JwtRequest();
			$this->message = $jwtMessage->decodeAndDecrypt( file_get_contents ( "php://input" ) );
			
			$this->log->out ( file_get_contents ( "php://input" ) );
		
			//$this->log->dump ( $this->message ,'The revived producer message');
			
			//propagate message
			return $this->dispatch($this->message, function($consumerResponse){
				$this->log->trace('handleMessage:: recieved response from JWT Consumer for dispatched message ');
				//$this->log->dump($consumerResponse, 'ProxyJwt: consumer response');
				
				$this->log->trace('handleMessage:: sending consumer-response back to the JWT Producer ');
				
				$this->response = new JwtResponse();
				$this->response->revive( (array) $consumerResponse);
				
				//$this->log->dump($this->response, 'ProxyJwt: revived consumer response');
				return $this->response->sendJSON();
			});
				
			
		} catch ( Exception $e ) {
			ExceptionHelper::sendExceptionJsonResponse ( new ParseException ( 'ProxyJwt: Message has wrong format: ' . $e->getMessage () ) );
		}
	}


	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){

	}


	/**
	 * Create a testmessage
	 */
	public function createMessage(){
		print __FUNCTION__ . ":\n";
		$this->log->debug('Producer::createMessage  Creating the test message');

		$dto = DataGenerator::getElwPortaalUser();//testdata, normally this is provided by the Producer application
		$message = new EsbMessage('PortaalUser', 'A Portaal User DTO instance', $this->auth->applicationKey);
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
		Out::dump($dto,'The PortaalUser test DTO');

		$message->setPayload($dto);

		return $message;
			
	}


}

/**
 * The proxy application
 */



$conn = new ProxyJwt();
$conn->handleMessage();




?>