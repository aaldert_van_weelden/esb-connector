<?php
require_once realpath(__DIR__.'/../..').'/application.php';

use Utils\Logger\LoggerInstance;
use Utils\Util;
use EsbCore\Connector\EsbConsumer;
use XHR\HTTPcodes;
use XHR\JSON;


/**
 * Dummy Consumer application, used to recieve test messages from the ESB Pipeline
 */

class Consumer extends EsbConsumer{
	
	const TAG = 'Consumer';
	
	/*
	 * TEST FLAGS
	 */
	const FAILURE_TEST = false;
	
	private $log;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		$this->message = new EsbMessage();
		
	}
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbConsumerInterface::setStatus()
	 */
	public function setStatus(){
		
	}
	
	/**
	 *
	 * {@inheritdoc}
	 *
	 * @see \EsbCore\Connector\EsbConsumerInterface::handleMessage()
	 */
	public function handleMessage(callable $callback=null) {
		Util::dump ( file_get_contents ( "php://input" ) , '"Consumer: php://input dump');
		$this->log->trace('handleMessage:: trying to authenticate the user');
		$this->response = new EsbResponse ( 'Consumer' );
		
		try {
			$this->message->revive ( json_decode ( file_get_contents ( "php://input" ), true ) );
			
			$this->log->dump ( $this->message );
			
			$this->response->setMessageID ( $this->message->getID () );
			$this->response->setPayload( (object) ['authorized'=>true]);
			$this->response->setStatus ( new EsbResponseStatus ( EsbResponseStatus::HTTP_ACCEPTED ) );
			$this->response->sendJSON(JSON::MOVE_ON, HTTPcodes::HTTP_OK );
		} catch ( Exception $e ) {
			ExceptionHelper::sendExceptionJsonResponse ( new ParseException ( 'Message has wrong format: ' . $e->getMessage () ) );
		}
	}
}

/********************************** TEST CONSUMER APPLICATION ******************************************
 * 
 */

$consumer = new Consumer();
$consumer->handleMessage();
?>