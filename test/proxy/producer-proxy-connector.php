<?php
require_once realpath(__DIR__.'/../..').'/application.php';
require_once 'data/DataGenerator.php';

use Utils\Logger\LoggerInstance;
use Utils\Util;
use EsbCore\Connector\EsbProducer;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use EsbCore\Entities\EnvironmentKey;

/**
 * Producer dummy application, used to send test messages to the ESB Pipeline
 */

class ProducerProxyConnector extends EsbProducer{
	
	const TAG = 'Producer';
	
	const KEY = 'PRODUCER';
	const TOKEN = '1234';
	
	private $log;
	private $proxyEndpoint;
	
	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);
		
		$this->proxyEndpoint = 'http://esb-connector.localhost/proxy/proxy.php';
		$this->auth = new CurlAuth(self::KEY, self::TOKEN);
	}
	
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch(\EsbMessage $message, callable $callback=null){
		print __FUNCTION__ . ":\n";
		$this->log->debug('Producer::dispatch Trying to send the test message with id['.$message->getID().']');
		
		$connector = new CurlHelper(CurlMethod::POST,$this->proxyEndpoint, $message, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();
		
		//$response = $connector->getResponse();
		$response = $connector->getResponseObject();
		
		return $response;
		
	}
	
	
	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){

	}
	
	
	/**
	 * Create a testmessage
	 */
	public function createMessage(){
		print __FUNCTION__ . ":\n";
		$this->log->debug('Producer::createMessage  Creating the test message');

		$dto = DataGenerator::getElwPortaalUser();//testdata, normally this is provided by the Producer application
		$message = new EsbMessage('PortaalUser', 'A Portaal User DTO instance', $this->auth->applicationKey);
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
		Util::dump($dto,'The PortaalUser test DTO');
		
		$message->setPayload($dto);
		
		return $message;
			
	}
	
	
}

?>