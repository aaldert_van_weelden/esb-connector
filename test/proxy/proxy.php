<?php
require_once realpath(__DIR__.'/../..').'/application.php';


use Utils\Logger\LoggerInstance;
use EsbCore\Entities\EnvironmentKey;
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;
use XHR\JSON;
use XHR\HTTPcodes;
/**
 * Producer dummy application, used to send test messages to the ESB Pipeline
 */

class Proxy{

	const TAG = 'Proxy';

	const KEY = 'PROXY';
	const TOKEN = 'ABCD';

	private $log;
	private $auth;
	private $consumerEndpoint;
	private $response;

	public function __construct(){
		$this->log = new LoggerInstance(self::TAG);

		$this->consumerEndpoint = 'http://esb-connector.localhost/proxy/consumer-connector.php';
		$this->auth = new CurlAuth(self::KEY, self::TOKEN);
		
		$this->log->trace('Initializing the Proxy');
	}



	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::dispatch()
	 */
	public function dispatch(\EsbMessage $message, $callback=null){
		print __FUNCTION__ . ":\n";
		$this->log->debug('Producer::dispatch Trying to send the test message with id['.$message->getID().']');

		$connector = new CurlHelper(CurlMethod::POST,$this->consumerEndpoint, $message, $this->auth, $_ENV[EnvironmentKey::FIDDLER_PROXY]);
		$connector->execute();

		//$response = $connector->getResponse();
		$consumerResponse = $connector->getResponseObject();

		return call_user_func($callback,$consumerResponse);

	}
	
	
	public function handleMessage($callback=null) {
		$this->log->trace('handleMessage:: trying to handle the message and propagate it to the consumer['.$this->consumerEndpoint.']');
		$this->log->dump ( file_get_contents ( "php://input" ) , '"Proxy: php://input dump');
		$this->response = new EsbResponse ( 'Consumer' );
	
		try {
			$this->message = new EsbMessage('');
			
			$this->message->revive ( json_decode ( file_get_contents ( "php://input" ), true ) );
					
			$this->log->dump ( $this->message );
			
			//propagate message
			return $this->dispatch($this->message, function($consumerResponse){
				
				$this->log->trace('handleMessage:: recieved response from Consumer for dispatched message ');
				$this->log->dump($consumerResponse);
				
				$this->log->trace('handleMessage:: sending response to the Producer ');
				
				$this->response->revive( (array) $consumerResponse);
				return $this->response->sendJSON(JSON::MOVE_ON, HTTPcodes::HTTP_OK );
			});
				
			
		} catch ( Exception $e ) {
			ExceptionHelper::sendExceptionJsonResponse ( new ParseException ( 'Message has wrong format: ' . $e->getMessage () ) );
		}
	}


	/**
	 * {@inheritDoc}
	 * @see \EsbCore\Connector\EsbProducerInterface::getConsumerStatus()
	 */
	public function getConsumerStatus($messageID){

	}


	/**
	 * Create a testmessage
	 */
	public function createMessage(){
		print __FUNCTION__ . ":\n";
		$this->log->debug('Producer::createMessage  Creating the test message');

		$dto = DataGenerator::getElwPortaalUser();//testdata, normally this is provided by the Producer application
		$message = new EsbMessage('PortaalUser', 'A Portaal User DTO instance', $this->auth->applicationKey);
		$message->setStatus(new ProducerMessageStatus (ProducerMessageStatus::TEST));
		Out::dump($dto,'The PortaalUser test DTO');

		$message->setPayload($dto);

		return $message;
			
	}


}

/**
 * The proxy application
 */



$conn = new Proxy();
$conn->handleMessage();




?>