<?php
use EsbCore\Connector\ProducerStatus\ProducerMessageStatus;

require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';
require_once 'proxy/producer-proxy-connector.php';

ini_set('html_errors', false);

/**
 * Test the proxy class here
 * 
 * @author Aaldert van Weelden
 *
 */
class ProxyTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line('------------------------------------------------');
	}
	
	
	
	
	
	
	
	
	
	public function testProducerToConsumerByProxy(){
		Out::print_line(__FUNCTION__);
		$connect = new ProducerProxyConnector();
		$message = $connect->createMessage();
		$response = $connect->dispatch($message);
		
		Out::dump($response,'The Consumer response by Proxy');
		
		$this->assertNotNull($response);
		$this->assertTrue(is_object($response));
		
		$refreshed = new EsbResponse();
		$refreshed->revive( (array) $response);
		
		$this->assertEqual(get_class($refreshed), 'EsbResponse');
		$this->assertNotNull($refreshed->getStatus());
		$this->assertEqual($refreshed->getStatus()->value, EsbResponseStatus::HTTP_ACCEPTED);
		
		$this->assertTrue($refreshed->getPayload()->authorized);
		
		
		
	}
		
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new ProxyTest())->run();


?>