<?php
require_once realpath(__DIR__.'/..').'/application.php';
require_once 'data/DataGenerator.php';
require_once 'proxy-jwt/producer-proxyjwt-connector.php';

ini_set('html_errors', false);

/**
 * Test the proxy JWT class here
 * 
 * @author Aaldert van Weelden
 *
 */
class ProxyJwtTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line('------------------------------------------------');
	}
	
	
	
	
	
	
	
	
	
	public function testProducerToConsumerByProxyJwt(){
		Out::print_line(__FUNCTION__);
		$connect = new ProducerProxyJwtConnector();
		$message = $connect->createMessage();
		$response = $connect->dispatch($message);
		
		Out::dump($response,'The ConsumerJwt response by ProxyJwt');
		
		$this->assertNotNull($response);
		$this->assertTrue(is_object($response));
		
		$this->assertEqual(get_class($response), 'EsbResponse');
		$this->assertNotNull($response->getStatus());
		$this->assertEqual($response->getStatus()->value, EsbResponseStatus::HTTP_ACCEPTED);
		
		$this->assertTrue($response->getPayload()->authorized);
		
		
	
	}
		
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new ProxyJwtTest())->run();


?>