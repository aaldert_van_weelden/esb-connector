-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 30 Mar 2013 om 13:16
-- Serverversie: 5.1.55
-- PHP-Versie: 5.3.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `orm_project_test`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `adres`
--

CREATE TABLE IF NOT EXISTS `adres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internal_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `internal_version` int(11) NOT NULL DEFAULT '0',
  `postcode` varchar(255) COLLATE utf8_bin NOT NULL,
  `plaats` varchar(255) COLLATE utf8_bin NOT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `timezone` int(11) NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `internal_id` (`internal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

--
-- Gegevens worden uitgevoerd voor tabel `adres`
--


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internal_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `adres_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `internal_version` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(255) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `permissions` text COLLATE utf8_bin,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `timezone` int(11) NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `internal_id` (`internal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

--
-- Gegevens worden uitgevoerd voor tabel `person`
--


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internal_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `internal_version` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `role` int(2) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `timezone` int(11) NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `internal_id` (`internal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

--
-- Gegevens worden uitgevoerd voor tabel `subject`
--


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internal_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `person_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `internal_version` int(11) NOT NULL DEFAULT '0',
  `login` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `permissions` text COLLATE utf8_bin,
  `updated` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `timezone` int(11) NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `internal_id` (`internal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

--
-- Gegevens worden uitgevoerd voor tabel `user`
--

