<?php
use phpseclib\Crypt;
use EsbCore\Entities\EnvironmentKey;

require_once '../application.php';

ini_set('html_errors', false);

/**
 * Test the phpseclib class here
 * 
 * @author Aaldert van Weelden
 *
 */
class RsaTest extends UnitTestCase {
	
	
	
	public function __construct(){
		parent::__construct();
		Out::print_underline(dirname(__FILE__).DIRECTORY_SEPARATOR.get_class($this));
	}
	
	public function __destruct(){
		
	}
	
	/**
	 * Called before every test
	 */
	public function setup(){
		
	}
	
	/**
	 * Called after every test
	 */
	public function teardown(){
		Out::print_line('------------------------------------------------');
	}
	
	
	
	
	
	
	
	
	
	public function testAESencrypt(){
		Out::print_line(__FUNCTION__);
		
		$cipher = new Crypt_AES(); // could use CRYPT_AES_MODE_CBC
		// keys are null-padded to the closest valid size
		// longer than the longest key and it's truncated
		//$cipher->setKeyLength(128);
		$cipher->setKey('abcdefghijklmnop');
		// the IV defaults to all-NULLs if not explicitly defined
		$cipher->setIV(crypt_random_string($cipher->block_size >> 3));
		
		$size = 10;
		$plaintext = str_repeat('a', $size);
		
		$encrypted = $cipher->encrypt($plaintext);
		Out::print_line('encrypted:');
		Out::print_line( bin2hex($encrypted) );
		
		Out::print_line('decrypted:');
		$cipher->setKey('abcdefghijklmnop');
		$decrypted = $cipher->decrypt($encrypted);
		Out::print_line( $decrypted );
		
		$this->assertEqual($decrypted, $plaintext);
		
		$cipher->setKey('anotherkey');
		$decrypted = $cipher->decrypt($encrypted);
		
		$this->assertFalse($decrypted);
		
		
		$encrypted = $cipher->encrypt($plaintext);
		$encrypted = bin2hex($encrypted);
		Out::print_line('encrypted:');
		Out::print_line( $encrypted );
		

		Out::print_line('decrypted:');
		$encrypted = hex2bin($encrypted);
		$decrypted = $cipher->decrypt($encrypted);
		Out::print_line( $decrypted );
		
		$this->assertEqual($decrypted, $plaintext);
		
		
	}
	
	
	public function testRSAencrypt(){
		$rsa = new Crypt_RSA();
		extract($rsa->createKey());
		
		$plaintext = 'testing1234';
		
		$rsa->loadKey($publickey);
		Out::print_line('public key:');
		Out::print_line($publickey);
		Out::print_line('private key:');
		Out::print_line($privatekey);
		
		Out::print_line('encrypted:');
		
		$ciphertext = $rsa->encrypt($plaintext);
		Out::print_line( utf8_encode($ciphertext) );
		
		$rsa->loadKey($privatekey);
		
		Out::print_line('decrypted:');
		$decrypted = $rsa->decrypt($ciphertext);
		Out::print_line($decrypted);
		
		$this->assertEqual($decrypted, $plaintext);
	
	}
	
	public function testRSAencryptWithKeyfiles(){
		$rsa = new Crypt_RSA();
	
		$plaintext = 'testingonetwo';
	
		$publickey = file_get_contents($_ENV[EnvironmentKey::RSA_KEYFILES].'id_rsa.pub');
		$privatekey = file_get_contents($_ENV[EnvironmentKey::RSA_KEYFILES].'id_rsa');
	
		$loaded = $rsa->loadKey($publickey);
		$this->assertTrue($loaded);
		
		Out::print_line('public key:');
		Out::print_line($publickey);
		Out::print_line('private key:');
		Out::print_line($privatekey);
	
		Out::print_line('encrypted:');
	
		$ciphertext = $rsa->encrypt($plaintext);
		Out::print_line( utf8_encode( $ciphertext ) );
	
		$loaded = $rsa->loadKey($privatekey);
		$this->assertTrue($loaded);
	
		Out::print_line('decrypted:');
		$decrypted = $rsa->decrypt($ciphertext);
		Out::print_line($decrypted);
		
		
		
		$this->assertEqual($decrypted, $plaintext);
		
		$loaded = $rsa->loadKey(__DIR__.'data/id_rsa');
		
		$this->assertFalse($loaded);
		
		
	
	}
		
}

if(isset($GLOBALS[TestSuite::INSTANTIATED])) return;

(new RsaTest())->run();


?>