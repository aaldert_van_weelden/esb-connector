<?php
require_once realpath(__DIR__.'/..').'/vendor/autoload.php';
require_once realpath(__DIR__.'/..').'/application.php';

/**
 * Aggregate global application testsuites here to run them all
 * 
 * @author Aaldert van Weelden
 *
 */
class GeneralTests extends TestSuite {
	
    public function __construct() {
    	parent::__construct();
        $this->TestSuite(get_class($this));
        
        $this->collect(dirname(__FILE__), new SimplePatternCollector('/_test.php/'));
    }
}

$testsuite = new GeneralTests();
$testsuite->run();
?>